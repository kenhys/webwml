#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag pagetitle>데비안 9 업데이트: 9.7 릴리스</define-tag>
<define-tag release_date>2019-01-23</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.7</define-tag>

<define-tag dsalink><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 안정 배포 데비안 <release> (코드명 <q><codename></q>)의 일곱번째 업데이트를 알리게 되어 기쁩니다.
이 포인트 릴리스는 <srcpkg APT>의 최근 보안 업데이트를 포함하여,
<codename>의 새 설치가 취약하지 않도록 돕습니다. 다른 업데이트는 포함하지 않습니다.
</p>

<p>새 설치 이미지는 일반 위치에서 곧 가능하게 될 겁니다.
</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors. Due to
the nature of the included updates, in this case it is recommended to follow the
instructions listed in <dsalink 2019 4371>.</p>

<p>미러의 완전한 목록은 아래에 있습니다:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>여러가지 버그 수정</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction base-files "Update for the point release">
</table>

<h2>보안 업데이트</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2019 4371 apt>
</table>

<h2>URL</h2>

<p>이 개정판으로 바뀐 패키지 전체 목록:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>안정 배포에 대한 제안된 업데이트:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정 배포 정보 (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>연락처 정보</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


