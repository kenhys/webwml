#use wml::debian::translation-check translation="c5e6056b6cfe4c47b624ccc6e664ff19613d23b7" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i gdk-pixbuf, GDK's Pixbuf-bibliotek.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44648">CVE-2021-44648</a>

    <p>Sahil Dhar rapporterede om en heapbaseret bufferoverløbssårbarhed, når 
    der blev dekodet en lzw-komprimeret billedatastream, hvilket kunne medføre 
    udførelse af vilkårlig kode eller lammelsesangreb, hvis et misdannet 
    GIF-billede blev behandlet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46829">CVE-2021-46829</a>

    <p>Pedro Ribeiro rapporterede om en heapbaseret bufferoverløbssårbarhed, 
    ved compositing eller clearing af frames i GIF-filer, hvilket kunne medføre 
    udførelse af vilkårlig kode eller lammelsesangreb, hvis misdannede 
    GIF-billeder blev behandlet.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 2.42.2+dfsg-1+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine gdk-pixbuf-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende gdk-pixbuf, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/gdk-pixbuf">\
https://security-tracker.debian.org/tracker/gdk-pixbuf</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5228.data"
