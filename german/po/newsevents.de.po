# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2008.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2011, 2012, 2016.
# Holger Wansing <linux@wansing-online.de>, 2014, 2015, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2022-05-23 20:46+0200\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Debian-Nachrichten"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Neueste Nachrichten von Debian"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "p<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "Der Newsletter für die Debian-Gemeinschaft"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Titel:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Autor:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Sprache:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Datum:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Veranstaltung:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Folien:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "Quellcode"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Zusammenfassung"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Kommende Veranstaltungen"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "Link könnte veraltet sein"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Wann"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Wo"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Weitere Informationen"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Beteiligung von Debian"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Hauptkoordinator"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Projekt</th><th>Koordinator</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Weitere Links"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Neueste Nachrichten"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Kalendereintrag herunterladen"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Zurück: Zu anderen <a href=\"./\">Debian-Neuigkeiten</a> || Zur <a href="
"\"m4_HOME/\">Homepage des Debian-Projekts</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (toter Link)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Willkommen zur <get-var issue /> Ausgabe der DPN in diesem Jahr, dem "
"Newsletter für die Debian-Gemeinschaft. Diese Ausgabe umfasst folgende "
"Themen:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
#| msgid ""
#| "Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
#| "the Debian community. Topics covered in this issue include:"
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Willkommen zur <get-var issue /> Ausgabe der DPN in diesem Jahr, dem "
"Newsletter für die Debian-Gemeinschaft."

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr "Andere Themen in dieser Ausgabe:"

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Gemäß der <a href=\"https://udd.debian.org/bugs.cgi\">Schnittstelle zur "
"Suche nach Fehlern in der Ultimate Debian Database</a> ist die kommende "
"Veröffentlichung, Debian <q><get-var release /></q>, derzeit von <get-var "
"testing /> veröffentlichungs-kritischen Fehlern (RC bugs) betroffen. Wenn "
"man die Fehler, die einfach behoben werden können oder bereits auf dem Weg "
"sind, behoben zu werden, ignoriert, müssen grob gerechnet noch rund <get-var "
"tobefixed /> veröffentlichungs-kritische Fehler behoben werden, damit die "
"Veröffentlichung stattfinden kann."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Es gibt auch noch <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">Hinweise dazu, wie Sie diese Zahlen interpretieren</a> müssen."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Es existieren auch noch <a href=\"<get-var url />\">genauere Statistiken</a> "
"sowie einige <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">Hinweise dazu, wie Sie diese Zahlen interpretieren</a> müssen."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\">Im Moment</a> sind <a href=\"m4_DEVEL/wnpp/"
"orphaned\"><get-var orphaned /> Pakete verwaist</a> und <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> Pakete warten auf Adoption</a>: Bitte sehen Sie "
"sich die vollständige Liste von <a href=\"m4_DEVEL/wnpp/help_requested"
"\">Paketen an, die Ihre Hilfe brauchen</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Bitte helfen Sie uns bei der Erstellung dieses Newsletters. Wir brauchen "
"mehr freiwillige Autoren, die die Debian-Gemeinschaft beobachten und "
"interessante Dinge darüber berichten. Bitte schauen Sie sich die <a href="
"\"https://wiki.debian.org/ProjectNews/HowToContribute\">HowToContribute-Wiki-"
"Seite</a> an, um sich darüber zu informieren, wie Sie uns helfen können. Wir "
"freuen uns, auf der Mailingliste <a href=\"mailto:debian-publicity@lists."
"debian.org\">debian-publicity@lists.debian.org</a> von Ihnen zu hören."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Bitte beachten Sie, dass dies eine Auswahl aus den wichtigsten "
"Sicherheitsankündigungen der letzten Wochen ist. Falls Sie bezüglich der vom "
"Debian-Security-Team veröffentlichten Ankündigungen auf dem aktuellsten "
"Stand sein müssen, abonnieren Sie bitte die <a href=\"<get-var url-dsa />"
"\">Security-Mailingliste</a> (und die separate <a href=\"<get-var url-bpo />"
"\">backports-Liste</a> sowie die <a href=\"<get-var url-stable-announce />"
"\">stable-updates-Liste</a>), damit Sie die entsprechenden Ankündigungen "
"erhalten."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Bitte beachten Sie, dass dies eine Auswahl aus den wichtigsten "
"Sicherheitsankündigungen der letzten Wochen ist. Falls Sie bezüglich der vom "
"Debian-Security-Team veröffentlichten Ankündigungen auf dem aktuellsten "
"Stand sein müssen, abonnieren Sie bitte die <a href=\"<get-var url-dsa />"
"\">Security-Mailingliste</a> (und die separate <a href=\"<get-var url-bpo />"
"\">backports-Liste</a> sowie die <a href=\"<get-var url-stable-announce />"
"\">stable-updates-Liste</a> oder die <a href=\"<get-var url-volatile-"
"announce />\">volatile-Liste</a> für <q><get-var old-stable /></q>, die "
"Oldstable-Distribution), damit Sie die entsprechenden Ankündigungen erhalten."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Debians Stable-Release-Team hat eine Aktualisierungs-Ankündigung "
"veröffentlicht für das Paket:"

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr " und "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Bitte lesen Sie sie sorgfältig und reagieren Sie entsprechend."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr ""
"Debians Backports-Team hat für diese Pakete Empfehlungen veröffentlicht:"

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Debians Security-Team hat kürzlich für diese Pakete (neben weiteren) "
"Empfehlungen veröffentlicht:"

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> Pakete wurden kürzlich zum Debian-unstable-Archiv "
"hinzugefügt."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr ""
" <a href=\"<get-var url-newpkg />\">Neben vielen weiteren sind dies</a>:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr ""
"Es gibt mehrere Veranstaltungen in nächster Zeit, die mit Debian "
"zusammenhängen:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Sie finden weitere Informationen über Debian betreffende Veranstaltungen und "
"Vorträge im <a href=\"<get-var events-section />\">Veranstaltungs-Bereich</"
"a> der Debian-Website, oder abonnieren Sie eine der Veranstaltungs-"
"Mailinglisten für die verschiedenen Regionen: <a href=\"<get-var events-ml-"
"eu />\">Europa</a>, <a href=\"<get-var events-ml-nl />\">die Niederlande</"
"a>, <a href=\"<get-var events-ml-ha />\">Lateinamerika</a>, <a href=\"<get-"
"var events-ml-na />\">Nordamerika</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Möchten Sie einen Debian-Stand oder eine Debian-Installationsparty "
"organisieren? Wissen Sie von einer bevorstehenden Veranstaltung, die Debian "
"betrifft? Haben Sie einen Vortrag über Debian gehalten, für den Sie auf der "
"<a href=\"<get-var events-talks />\">Seite über Debian-Vorträge</a> einen "
"Link hinzufügen möchten? Senden Sie eine E-Mail (auf Englisch) an das <a "
"href=\"<get-var events-team />\">Debian-Events-Team</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> Bewerber wurden als Debian-Entwickler <a href=\"<get-var "
"dd-url />\">akzeptiert</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> Bewerber wurden als Debian-Betreuer <a href=\"<get-var dm-"
"url />\">akzeptiert</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num /> Leute haben <a href=\"<get-var uploader-url />"
"\">begonnen, Pakete zu betreuen</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> seit der letzten Veröffentlichung "
"der Debian-Projekt-News. Bitte heißen Sie <get-var eval-newcontributors-name-"
"list /> in unserem Projekt willkommen!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"Die <get-var issue-devel-news /> Ausgabe der <a href=\"<get-var url-devel-"
"news />\">unterschiedlichen Neuigkeiten für Entwickler</a> wurde "
"veröffentlicht und enthält folgende Themen:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Wenn Sie diesen Newsletter (auf Englisch) in Ihrer Mailbox haben wollen, <a "
"href=\"https://lists.debian.org/debian-news/\">abonnieren Sie die "
"Mailingliste debian-news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"Hier gibt es <a href=\"../../\">ältere Ausgaben</a> dieser Nachrichtenseite."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Die Nachrichten des Debian-Projekts werden von <a href=\"mailto:debian-"
"publicity@lists.debian.org\">%s</a> erstellt."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Die Nachrichten des Debian-Projekts werden von <a href=\"mailto:debian-"
"publicity@lists.debian.org\">%s</a> erstellt."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Diese Ausgabe der Nachrichten für das Debian-Projekt wurde von <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a> erstellt."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"Diese Ausgabe der Nachrichten für das Debian-Projekt wurde von <a href="
"\"mailto:debian-publicity@lists.debian.org\">%s</a> erstellt."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr "Sie wurde von %s übersetzt."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr "Sie wurde von %s übersetzt."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "Sie wurde von %s übersetzt."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "Sie wurde von %s übersetzt."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Wenn Sie diesen Newsletter wöchentlich in Ihrer Mailbox haben wollen, <a "
"href=\"https://lists.debian.org/debian-news-german/\">abonnieren Sie die "
"Mailingliste debian-news-german</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Die wöchentlichen Debian-Nachrichten werden von <a href=\"mailto:dwn@debian."
"org\">%s</a> erstellt."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Die wöchentlichen Debian-Nachrichten werden von <a href=\"mailto:dwn@debian."
"org\">%s</a> erstellt."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"Diese Ausgabe der wöchentlichen Debian-Nachrichten wurde von <a href="
"\"mailto:dwn@debian.org\">%s</a> erstellt."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"Diese Ausgabe der wöchentlichen Debian-Nachrichten wurde von <a href="
"\"mailto:dwn@debian.org\">%s</a> erstellt."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Zurück zu <a href=\"./\">Debians Seite der Vortragenden</a>."

#~ msgid "Email:"
#~ msgstr "E-Mail:"

#~ msgid "Languages:"
#~ msgstr "Sprachen:"

#~ msgid "List of Speakers"
#~ msgstr "Liste der Vortragenden"

#~ msgid "Location:"
#~ msgstr "Wohnort:"

#~ msgid "Name:"
#~ msgstr "Name:"

#~ msgid "Previous Talks:"
#~ msgstr "Vorherige Vorträge:"

#~ msgid "Topics:"
#~ msgstr "Themen:"
