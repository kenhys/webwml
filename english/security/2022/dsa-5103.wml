<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered that the BN_mod_sqrt() function of OpenSSL
could be tricked into an infinite loop. This could result in denial of
service via malformed certificates.</p>

<p>Additional details can be found in the upstream advisory:
<a href="https://www.openssl.org/news/secadv/20220315.txt">\
https://www.openssl.org/news/secadv/20220315.txt</a></p>

<p>In addition this update corrects a carry propagation bug specific to
MIPS architectures.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 1.1.1d-0+deb10u8.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.1.1k-1+deb11u2.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5103.data"
# $Id: $
