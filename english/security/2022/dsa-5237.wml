<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code, CSP bypass or session fixation.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 91.x series has ended, so starting with this update we're now
following the 102.x releases.</p>

<p>Between 91.x and 102.x, Firefox has seen a number of feature updates.
For more information please refer to
<a href="https://www.mozilla.org/en-US/firefox/102.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/102.0esr/releasenotes/</a></p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 102.3.0esr-1~deb11u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5237.data"
# $Id: $
