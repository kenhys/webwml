<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

    <p>Kirill Tkhai discovered a data leak in the way the XFS_IOC_ALLOCSP
    IOCTL in the XFS filesystem allowed for a size increase of files
    with unaligned size. A local attacker can take advantage of this
    flaw to leak data on the XFS filesystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

    <p>Juergen Gross reported that malicious PV backends can cause a denial
    of service to guests being serviced by those backends via high
    frequency events, even if those backends are running in a less
    privileged environment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

    <p>Juergen Gross discovered that Xen guests can force the Linux
    netback driver to hog large amounts of kernel memory, resulting in
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

    <p>Szymon Heidrich discovered a buffer overflow vulnerability in the
    USB gadget subsystem, resulting in information disclosure, denial of
    service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

    <p>It was discovered that the Phone Network protocol (PhoNet) driver
    has a reference count leak in the pep_sock_accept() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

    <p>Wenqing Liu reported an out-of-bounds memory access in the f2fs
    implementation if an inode has an invalid last xattr entry. An
    attacker able to mount a specially crafted image can take advantage
    of this flaw for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

    <p>A memory leak flaw was discovered in the __rds_conn_create()
    function in the RDS (Reliable Datagram Sockets) protocol subsystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0185">CVE-2022-0185</a>

    <p>William Liu, Jamie Hill-Daniel, Isaac Badipe, Alec Petridis, Hrvoje
    Misetic and Philip Papurt discovered a heap-based buffer overflow
    flaw in the legacy_parse_param function in the Filesystem Context
    functionality, allowing an local user (with CAP_SYS_ADMIN capability
    in the current namespace) to escalate privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23222">CVE-2022-23222</a>

    <p><q>tr3e</q> discovered that the BPF verifier does not properly restrict
    several *_OR_NULL pointer types allowing these types to do pointer
    arithmetic. A local user with the ability to call bpf(), can take
    advantage of this flaw to excalate privileges. Unprivileged calls to
    bpf() are disabled by default in Debian, mitigating this flaw.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.92-1. This version includes changes which were aimed to
land in the next Debian bullseye point release.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5050.data"
# $Id: $
