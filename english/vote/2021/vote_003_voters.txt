-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1             abe	Axel Beckert
    2        achernya	Alexander Chernyakhovsky
    3            adsb	Adam D. Barratt
    4           alexm	Alex Muntada
    5        amacater	Andrew Martin Adrian Cater
    6         anarcat	Antoine Beaupré
    7            anbe	Andreas Beckmann
    8            andi	Andreas B. Mundt
    9        angdraug	Dmitry Borodaenko
   10          ansgar	Ansgar
   11             apo	Markus Koschany
   12              az	Alexander Zangerl
   13        azekulic	Alen Zekulic
   14      babelouest	Nicolas Mora
   15        ballombe	Bill Allombert
   16             bas	Bas Zoetekouw
   17          bbaren	Benjamin Barenblat
   18         bblough	William Blough
   19          bdrung	Benjamin Drung
   20            benh	Ben Hutchings
   21         bigeasy	Sebastian Andrzej Siewior
   22           bluca	Luca Boccassi
   23          boutil	Cédric Boutillier
   24         bremner	David Bremner
   25         broonie	Mark Brown
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26            bunk	Adrian Bunk
   27            bzed	Bernd Zeimetz
   28        calculus	Jerome Georges Benoit
   29          carnil	Salvatore Bonaccorso
   30           cavok	Domenico Andreoli
   31        cespedes	Juan Cespedes
   32       chronitis	Gordon Ball
   33        cjwatson	Colin Watson
   34           cklin	Chuan-kai Lin
   35           clint	Clint Adams
   36          csmall	Craig Small
   37           cwryu	Changwoo Ryu
   38          czchen	ChangZhuo Chen
   39          daniel	Daniel Baumann
   40           dannf	Dann Frazier
   41       debalance	Philipp Huebner
   42        deltaone	Patrick Franz
   43       dktrkranz	Luca Falavigna
   44          dlange	Daniel Lange
   45             dmn	Damyan Ivanov
   46             dod	Dominique Dumont
   47         dogsleg	Lev Lamberov
   48             don	Don Armstrong
   49         donkult	David Kalnischkies
   50       dtorrance	Douglas Andrew Torrance
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51        ehashman	Elana Hashman
   52          elbrus	Paul Mathijs Gevers
   53        emollier	Étienne Mollier
   54          enrico	Enrico Zini
   55            eric	Eric Dorland
   56           fabbe	Fabian Fagerholm
   57             faw	Felipe Augusto van de Wiel
   58           felix	Félix Sipma
   59         filippo	Filippo Giunchedi
   60         florian	Florian Ernst
   61         fpeters	Frederic Peters
   62        francois	Francois Marier
   63         gabriel	Gabriel F. T. Gomes
   64         gaudenz	Gaudenz Steinlin
   65        georgesk	Georges Khaznadar
   66         giovani	Giovani Augusto Ferreira
   67        glaubitz	John Paul Adrian Glaubitz
   68         godisch	Martin A. Godisch
   69          gregoa	Gregor Herrmann
   70            gspr	Gard Spreemann
   71         guilhem	Guilhem Moulin
   72         guillem	Guillem Jover
   73            guus	Guus Sliepen
   74           gwolf	Gunnar Wolf
   75        hartmans	Sam Hartman
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76           hefee	Sandro Knauß
   77         helmutg	Helmut Grohne
   78         hertzog	Raphaël Hertzog
   79      hlieberman	Harlan Lieberman-Berg
   80          holger	Holger Levsen
   81      hvhaugwitz	Hannes von Haugwitz
   82       intrigeri	Intrigeri
   83           ivodd	Ivo De Decker
   84           jandd	Jan Dittberner
   85          jaqque	John Robinson
   86         jbfavre	Jean Baptiste Favre
   87             jcc	Jonathan Cristopher Carter
   88            jcfp	Jeroen Ploemen
   89             jdg	Julian Gilbey
   90           jello	Joseph Nahmias
   91          jlines	John Lines
   92             jmw	Jonathan Wiltshire
   93           joerg	Joerg Jaspert
   94         joostvb	Joost van Baal
   95           jordi	Jordi Mallach
   96         joussen	Mario Joussen
   97       jpmengual	Jean-Philippe MENGUAL
   98        jredrejo	José L. Redrejo Rodríguez
   99        jspricke	Jochen Sprickerhof
  100       jvalleroy	James Valleroy
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101          kartik	Kartik Mistry
  102            knok	Takatsugu Nokubi
  103           kobla	Ondřej Kobližek
  104          koster	Kanru Chen
  105           krala	Antonin Kral
  106         kreckel	Richard Kreckel
  107      kritzefitz	Sven Bartscher
  108           laney	Iain Lane
  109         larjona	Laura Arjona Reina
  110        lavamind	Jerome Charaoui
  111        lawrencc	Christopher Lawrence
  112         ldrolez	Ludovic Drolez
  113         lechner	Felix Lechner
  114         legoktm	Kunal Mehta
  115         lenharo	Daniel Lenharo de Souza
  116             leo	Carsten Leonhardt
  117         lopippo	Filippo Rusconi
  118           lucab	Luca Bruno
  119         lyknode	Baptiste Beauplat
  120         madduck	Martin F. Krafft
  121         matthew	Matthew Vernon
  122          mattia	Mattia Rizzolo
  123            maxy	Maximiliano Curia
  124         mbehrle	Mathias Behrle
  125           mbuck	Martin Buck
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126            mejo	Jonas Meurer
  127          merker	Karsten Merker
  128        mgilbert	Michael Gilbert
  129           micha	Micha Lenk
  130           milan	Milan Kupcevic
  131           mones	Ricardo Mones Lastra
  132           mpitt	Martin Pitt
  133            myon	Christoph Berg
  134           neilm	Neil McGovern
  135           nickm	Nick Morrott
  136           noahm	Noah Meyerhans
  137         noodles	Jonathan McDowell
  138           ntyni	Niko Tyni
  139           ohura	Makoto OHURA
  140           olasd	Nicolas Dandrimont
  141            olek	Olek Wojnar
  142           osamu	Osamu Aoki
  143            otto	Otto Kekäläinen
  144    paddatrapper	Kyle Robbertze
  145          paride	Paride Legovini
  146             peb	Pierre-Elliott Bécue
  147             pgt	Pierre Gruet
  148           philh	Philip Hands
  149            pini	Gilles Filippini
  150             pjb	Phil Brooke
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151           pkern	Philipp Kern
  152          plessy	Charles Plessy
  153       pmatthaei	Patrick Matthäi
  154          pmhahn	Philipp Matthias Hahn
  155           pollo	Louis-Philippe Véronneau
  156             rak	Ryan Kavanagh
  157             ras	Russell Stuart
  158         rbalint	Balint Reczey
  159             rcw	Robert Woodcock
  160             reg	Gregory Colpart
  161         reichel	Joachim Reichel
  162         rlaager	Richard Laager
  163        roehling	Timo Röhling
  164        rousseau	Ludovic Rousseau
  165             rra	Russ Allbery
  166             seb	Sebastien Delafond
  167       sebastien	Sébastien Villemot
  168         serpent	Tomasz Rybak
  169              sf	Stefan Fritsch
  170           skitt	Stephen Kitt
  171             smr	Steven Michael Robbins
  172           smurf	Matthias Urlichs
  173       spwhitton	Sean Whitton
  174       sramacher	Sebastian Ramacher
  175            srud	Sruthi Chandran
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176        stappers	Geert Stappers
  177        stefanor	Stefano Rivera
  178  stephanlachnit	Stephan Lachnit
  179         stevenc	Steven Chamberlain
  180          stuart	Stuart Prescott
  181           sur5r	Jakob Haufe
  182          taffit	David Prévot
  183          takaki	Takaki Taniguchi
  184             tbm	Martin Michlmayr
  185        terceiro	Antonio Terceiro
  186              tg	Thorsten Glaser
  187           tiago	Tiago Bortoletto Vaz
  188          tianon	Tianon Gravi
  189            tiwe	Timo Weingärtner
  190        tjhukkan	Teemu Hukkanen
  191        tmancill	Tony Mancill
  192           troyh	Troy Heber
  193        tvincent	Thomas Vincent
  194            ucko	Aaron M. Ucko
  195        ukleinek	Uwe Kleine-König
  196           urbec	Judit Foglszinger
  197         vagrant	Vagrant Cascadian
  198           viiru	Arto Jantunen
  199            vivi	Vincent Prat
  200          vvidic	Valentin Vidic
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201          wagner	Hanno Wagner
  202          wouter	Wouter Verhelst
  203            wrar	Andrey Rahmatullin
  204             xam	Max Vozeler
  205            yadd	Xavier Guimard
  206            zack	Stefano Zacchiroli
  207            zhsj	Shengjing Zhu
  208            zigo	Thomas Goirand
