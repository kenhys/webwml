<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in rubygems embedded in
ruby2.1, the interpreted scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8320">CVE-2019-8320</a>

    <p>A Directory Traversal issue was discovered in RubyGems. Before
    making new directories or touching files (which now include
    path-checking code for symlinks), it would delete the target
    destination.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

    <p>The gem owner command outputs the contents of the API response
    directly to stdout. Therefore, if the response is crafted, escape
    sequence injection may occur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

    <p>Gem::GemcutterUtilities#with_response may output the API response to
    stdout as it is. Therefore, if the API side modifies the response,
    escape sequence injection may occur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

    <p>A crafted gem with a multi-line name is not handled correctly.
    Therefore, an attacker could inject arbitrary code to the stub line
    of gemspec, which is eval-ed by code in ensure_loadable_spec during
    the preinstall check.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

    <p>An issue was discovered in RubyGems 2.6 and later through 3.0.2.
    Since Gem::CommandManager#run calls alert_error without escaping,
    escape sequence injection is possible. (There are many ways to cause
    an error.)</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.5-2+deb8u7.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1735.data"
# $Id: $
