<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in modsecurity-apache, open source, cross
platform web application firewall (WAF) engine for Apache which allows
remote attackers to bypass the applications firewall and other
unspecified impact.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48279">CVE-2022-48279</a>

    <p>In ModSecurity before 2.9.6 and 3.x before 3.0.8, HTTP multipart
    requests were incorrectly parsed and could bypass the Web
    Application Firewall.
    NOTE: this is related to <a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a>
    but can be considered independent changes to the ModSecurity
    (C language) codebase.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24021">CVE-2023-24021</a>

    <p>Incorrect handling of null-bytes in file uploads in ModSecurity
    before 2.9.7 may allow for Web Application Firewall bypasses
    and buffer overflows on the Web Application Firewall
    when executing rules reading the FILES_TMP_CONTENT collection.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.9.3-1+deb10u2.</p>

<p>We recommend that you upgrade your modsecurity-apache packages.</p>

<p>For the detailed security status of modsecurity-apache please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/modsecurity-apache">https://security-tracker.debian.org/tracker/modsecurity-apache</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3283.data"
# $Id: $
