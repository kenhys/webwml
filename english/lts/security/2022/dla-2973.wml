<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in minidlna, a lightweight DLNA/UPnP-AV server
targeted at embedded systems. HTTP requests needed more checks to protect
against DNS rebinding, thus forbid a remote web server to exfiltrate
media files.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.1.6+dfsg-1+deb9u2.</p>

<p>We recommend that you upgrade your minidlna packages.</p>

<p>For the detailed security status of minidlna please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/minidlna">https://security-tracker.debian.org/tracker/minidlna</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2973.data"
# $Id: $
