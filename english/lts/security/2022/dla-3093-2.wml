<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA 3093-1 which included fix
for <a href="https://security-tracker.debian.org/tracker/CVE-2022-32224">CVE-2022-32224</a> caused a regression due to incompatibility with
ruby 2.5 version. We have dropped aforementioned fix. Updated rails
packages are now available.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:5.2.2.1+dfsg-1+deb10u5.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>For the detailed security status of rails please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3093-2.data"
# $Id: $
