<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential out-of-bounds read in the BGP
daemon of frr, a set of tools to route internet traffic.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37032">CVE-2022-37032</a>

    <p>An out-of-bounds read in the BGP daemon of FRRouting FRR before 8.4 may
    lead to a segmentation fault and denial of service. This occurs in
    bgp_capability_msg_parse in bgpd/bgp_packet.c.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
6.0.2-2+deb10u2.</p>

<p>We recommend that you upgrade your frr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3211.data"
# $Id: $
