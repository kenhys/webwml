<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>
It was discovered that there was a potential entity-expansion issue in
libjdom1-java, a lightweight and fast library for using XML. Attackers
could have caused a denial of service via a specially-crafted HTTP
request.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33813">CVE-2021-33813</a>

    <p>An XXE issue in SAXBuilder in JDOM through 2.0.6 allows attackers to
    cause a denial of service via a crafted HTTP request.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.1.3-1+deb9u1.</p>

<p>We recommend that you upgrade your libjdom1-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2712.data"
# $Id: $
