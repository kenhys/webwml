<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Stack-based buffer over-reads for crafted NTLM requests were fixed in
libntlm, a library that implements Microsoft's NTLM authentication</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4-8+deb9u1.</p>

<p>We recommend that you upgrade your libntlm packages.</p>

<p>For the detailed security status of libntlm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libntlm">https://security-tracker.debian.org/tracker/libntlm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2831.data"
# $Id: $
