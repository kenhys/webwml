<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Numerous vulnerabilities were discovered in ImageMagick, an image
manipulation program. Issues include memory exception, heap, buffer
and stack overflows, out of bound reads and missing checks.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u10.</p>

<p>The exact impact of the vulnerabilities is unknown, as they were
mostly discovered through fuzzing. We still recommend that you upgrade
your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-756.data"
# $Id: $
