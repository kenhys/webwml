<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in OpenSSL, a Secure Socket Layer
toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2105">CVE-2016-2105</a>

    <p>Guido Vranken discovered that an overflow can occur in the function
    EVP_EncodeUpdate(), used for Base64 encoding, if an attacker can
    supply a large amount of data. This could lead to a heap corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2106">CVE-2016-2106</a>

    <p>Guido Vranken discovered that an overflow can occur in the function
    EVP_EncryptUpdate() if an attacker can supply a large amount of data.
    This could lead to a heap corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2107">CVE-2016-2107</a>

    <p>Juraj Somorovsky discovered a padding oracle in the AES CBC cipher
    implementation based on the AES-NI instruction set. This could allow
    an attacker to decrypt TLS traffic encrypted with one of the cipher
    suites based on AES CBC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2108">CVE-2016-2108</a>

    <p>David Benjamin from Google discovered that two separate bugs in the
    ASN.1 encoder, related to handling of negative zero integer values
    and large universal tags, could lead to an out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2109">CVE-2016-2109</a>

    <p>Brian Carpenter discovered that when ASN.1 data is read from a BIO
    using functions such as d2i_CMS_bio(), a short invalid encoding can
    casuse allocation of large amounts of memory potentially consuming
    excessive resources or exhausting memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2176">CVE-2016-2176</a>

    <p>Guido Vranken discovered that ASN.1 Strings that are over 1024 bytes
    can cause an overread in applications using the X509_NAME_oneline()
    function on EBCDIC systems. This could result in arbitrary stack data
    being returned in the buffer.</p></li>

</ul>

<p>Additional information about these issues can be found in the OpenSSL
security advisory at <a href="https://www.openssl.org/news/secadv/20160503.txt">https://www.openssl.org/news/secadv/20160503.txt</a></p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in openssl version 1.0.1e-2+deb7u21</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-456.data"
# $Id: $
