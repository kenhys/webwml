<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some memory management issues were found in the GStreamer <q>bad</q> plugins:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5843">CVE-2017-5843</a>

    <p>A use after free issue was found in the mxfdemux element, which can
    can be triggered via a maliciously crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5848">CVE-2017-5848</a>

    <p>The psdemux was vulnerable to several invalid reads, which could be
    triggered via a maliciously crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.23-7.1+deb7u5.</p>

<p>We recommend that you upgrade your gst-plugins-bad0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-830.data"
# $Id: $
