<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Josef Gajdusek discovered two vulnerabilities in gtk-vnc, a VNC viewer
widget for GTK:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5884">CVE-2017-5884</a>

    <p>Fix bounds checking for RRE, hextile &amp; copyrec encodings. This bug
    allowed a remote server to cause a denial of service by buffer
    overflow via a carefully crafted message containing subrectangles
    outside the drawing area.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5885">CVE-2017-5885</a>

    <p>Correctly validate color map range indexes. This bug allowed a
    remote server to cause a denial of service by buffer overflow via
    a carefully crafted message with out-of-range colour values.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.0-3.1+deb7u1.</p>

<p>We recommend that you upgrade your gtk-vnc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-831.data"
# $Id: $
