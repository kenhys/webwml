<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The XML::LibXML perl module is affected by a "use-after-free"
vulnerability which allows remote attackers to execute arbitrary code by
controlling the arguments to a replaceChild() call.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0001+dfsg-1+deb7u2.</p>

<p>We recommend that you upgrade your libxml-libxml-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1171.data"
# $Id: $
