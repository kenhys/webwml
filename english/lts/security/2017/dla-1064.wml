<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Guido Vranken discovered that FreeRADIUS, an open source
implementation of RADIUS, the IETF protocol for AAA (Authorisation,
Authentication, and
Accounting), did not properly handle memory when processing packets.
This would allow a remote attacker to cause a denial-of-service by
application crash, or potentially execute arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.12+dfsg-1.2+deb7u2.</p>

<p>We recommend that you upgrade your freeradius packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1064.data"
# $Id: $
