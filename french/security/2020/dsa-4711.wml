#use wml::debian::translation-check translation="3ce93908ff9ed5d5edeae5cc5bcfa241d553b2f8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans coturn, un serveur
TURN et STUN pour VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4067">CVE-2020-4067</a>

<p>Felix Doerre a signalé que le tampon de réponse STUN n'était pas
correctement initialisé ce qui pourrait permettre à un attaquant de
divulguer des octets parmi les octets de remplissage à partir de la
connexion d'un autre client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6061">CVE-2020-6061</a>

<p>Aleksandar Nikolic a signalé qu'une requête POST HTTP contrefaite peut
conduire à des fuites d'informations et à un autre mauvais comportement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6062">CVE-2020-6062</a>

<p>Aleksandar Nikolic a signalé qu'une requête POST HTTP contrefaite peut
conduire au plantage du serveur et à un déni de service.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 4.5.0.5-1+deb9u2.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.5.1.1-1.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets coturn.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de coturn, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4711.data"
# $Id: $
