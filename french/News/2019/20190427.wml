#use wml::debian::translation-check translation="f797e8b952b201d953c4e7de221fd635808136e4" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.9</define-tag>
<define-tag release_date>2019-04-27</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la neuvième mise à jour de sa
distribution stable Debian <release> (nommée <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<p>Un cas particulier pour cette publication est que les personnes qui utilisent
l'outil <q>apt-get</q> pour réaliser leurs mises à niveau devront s'assurer
d'utiliser la commande <q>dist-upgrade</q>, afin de mettre à jour vers les paquets
les plus récents du noyau. Les utilisateurs d'autres outils tels que <q>apt</q> et
<q>aptitude</q> utiliseront la commande <q>upgrade</q>.</p>

<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction audiofile "Correction de problèmes de déni de service [CVE-2018-13440] et de dépassement de tampon [CVE-2018-17095]">
<correction base-files "Mise à jour pour cette version">
<correction bwa "Correction de dépassement de tampon [CVE-2019-10269]">
<correction ca-certificates-java "Correction de bashismes dans postinst et jks-keystore">
<correction cernlib "Application aux modules Fortran de l'option d'optimisation -O à la place de -O2 qui génère du code cassé ; correction d'échec de construction sur arm64 en désactivant PIE pour les exécutables Fortran">
<correction choose-mirror "Mise à jour de la liste de miroirs inclus">
<correction chrony "Correction de la journalisation des mesures et des statistiques, et arrêt de chronyd, sur certaines plateformes lorsque le filtrage seccomp est activé">
<correction ckermit "Suppression de la vérification de version d'OpenSSL">
<correction clamav "Correction d'accès au tas hors limites lors de l'analyse de documents PDF [CVE-2019-1787], de fichiers PE empaquetés avec Aspack [CVE-2019-1789] ou de fichiers OLE2 [CVE-2019-1788]">
<correction dansguardian "Ajout de <q>missingok</q> à la configuration de logrotate">
<correction debian-installer "Reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-security-support "Mise à jour des états de la prise en charge">
<correction diffoscope "Correction de tests pour qu'ils fonctionnent avec Ghostscript 9.26">
<correction dns-root-data "Mise à jour des données racine à 2019031302">
<correction dnsruby "Ajout de nouvelles clés racine (KSK-2017) ; abandon de TimeoutError, utilisation de Timeout::Error dans ruby 2.3.0 ">
<correction dpdk "Nouvelle version amont stable">
<correction edk2 "Correction de dépassement de tampon dans le service BlockIo [CVE-2018-12180] ; DNS : vérification de la taille des paquets reçus avant utilisation [CVE-2018-12178] ; correction de dépassement de pile avec des fichiers BMP corrompus [CVE-2018-12181]">
<correction firmware-nonfree "atheros/iwlwifi : mise à jour du microprogramme BlueTooth [CVE-2018-5383]">
<correction flatpak "Rejet de tous les contrôles d'entrées et de sorties que le noyau interpréterait comme TIOCSTI [CVE-2019-10063]">
<correction geant321 "Reconstruction avec cernlib avec des optimisations de Fortran corrigées">
<correction gnome-chemistry-utils "Arrêt de la construction du paquet gcu-plugin obsolète">
<correction gocode "gocode-auto-complete-el : promotion d'auto-complete-el pour Pre-Depends pour assurer des mises à niveau réussies">
<correction gpac "Correction de dépassement de tampons [CVE-2018-7752 CVE-2018-20762], de dépassements de tas [CVE-2018-13005 CVE-2018-13006 CVE-2018-20761], d'écritures hors limites [CVE-2018-20760 CVE-2018-20763]">
<correction icedtea-web "Arrêt de la construction du greffon du navigateur ne fonctionnant plus avec Firefox 60">
<correction igraph "Correction d'un plantage lors du chargement de fichiers GraphML mal formés [CVE-2018-20349]">
<correction jabref "Correction d'une attaque d’entités externes XML [CVE-2018-1000652]">
<correction java-common "Retrait du paquet default-java-plugin, dans la mesure où le greffon XUL icedtea-web a été retiré">
<correction jquery "Pollution de Object.prototype évitée [CVE-2019-11358]">
<correction kauth "Correction de gestion non sécurisée des arguments dans les assistants [CVE-2019-7443]">
<correction libdate-holidays-de-perl "Ajout du 8 mars (à partir de 2019) et du 8 mai (uniquement en 2020) comme jours fériés (à Berlin seulement)">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libreoffice "Introduction de la nouvelle ère japonaise « Reiwa » ; faire que -core entre en conflit avec openjdk-8-jre-headless (= 8u181-b13-2~deb9u1), qui a un ClassPathURLCheck cassé">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour de l'ABI du noyau vers la version -9">
<correction mariadb-10.1 "Nouvelle version amont stable">
<correction mclibs "Reconstruction avec cernlib avec des optimisations de Fortran corrigées">
<correction ncmpc "Correction d'un déréférencement de pointeur NULL [CVE-2018-9240]">
<correction node-superagent "Correction d'attaques de bombe de décompression [CVE-2017-16129] ; correction d'une erreur de syntaxe">
<correction nvidia-graphics-drivers "Nouvelle version amont stable [CVE-2018-6260]">
<correction nvidia-settings "Nouvelle version amont stable">
<correction obs-build "Pas de permission d'écriture de fichiers dans le système hôte [CVE-2017-14804]">
<correction paw "Reconstruction avec cernlib avec des optimisations de Fortran corrigées">
<correction perlbrew "URL HTTPS vers CPAN autorisées">
<correction postfix "Nouvelle version amont stable">
<correction postgresql-9.6 "Nouvelle version amont stable">
<correction psk31lx "Tri de version correct pour éviter de problèmes potentiels de mise à niveau">
<correction publicsuffix "Mise à jour des données incluses">
<correction pyca "Ajout de <q>missingok</q> à la configuration de logrotate">
<correction python-certbot "Retour à debhelper compat 9 pour s'assurer que les minuteurs de systemd sont correctement démarrés">
<correction python-cryptography "Retrait de BIO_callback_ctrl : le prototype diffère de sa définition par OpenSSL après qu'il a été modifié (corrigé) dans OpenSSL">
<correction python-django-casclient "Application de la correction de l'intergiciel django 1.10 ; python(3)-django-casclient : correction de dépendance manquante à python(3)-django">
<correction python-mode "Retrait de la prise en charge de xemacs21">
<correction python-pip "Captation correcte des HTTPError des requêtes dans index.py">
<correction python-pykmip "Correction d'un possible problème de déni de service [CVE-2018-1000872]">
<correction r-cran-igraph "Correction de déni de service au moyen d'un objet contrefait [CVE-2018-20349]">
<correction rails "Correction de problèmes de divulgation d'informations [CVE-2018-16476 CVE-2019-5418], d'un problème de déni de service [CVE-2019-5419]">
<correction rsync "Plusieurs corrections de sécurité pour zlib [CVE-2016-9840 CVE-2016-9841 CVE-2016-9842 CVE-2016-9843]">
<correction ruby-i18n "Vulnérabilité de déni de service distant évitée [CVE-2014-10077]">
<correction ruby2.3 "Correction d'échec de construction à partir des sources">
<correction runc "Correction d'une vulnérabilité par augmentation de droits administrateur [CVE-2019-5736]">
<correction systemd "journald : correction d'un échec d'assertion dans journal_file_link_data ; tmpfiles : correction de <q>e</q> pour prendre en charge des motifs génériques de l’interpréteur de commandes ; mount-util : acceptation de l'échec potentiel de name_to_handle_at() avec EPERM ; automount : acceptation des requêtes de montage automatique même quand il est déjà monté [CVE-2018-1049] ; correction d'une possible augmentation de droits administrateur [CVE-2018-15686]">
<correction twitter-bootstrap3 "Correction d'un problème de script intersite dans tooltips ou popovers [CVE-2019-8331]">
<correction tzdata "Nouvelle version stable">
<correction unzip "Correction de dépassement de tampon dans les archives ZIP protégées par mot de passe [CVE-2018-1000035]">
<correction vcftools "Correction d'une divulgation d'informations [CVE-2018-11099] et d'un déni de service [CVE-2018-11129 CVE-2018-11130] au moyen de fichiers contrefaits">
<correction vips "Correction de déréférencement de pointeur de fonction NULL [CVE-2018-7998] et d'accès mémoire non initialisé [CVE-2019-6976]">
<correction waagent "Nouvelle version stable avec plusieurs corrections d'Azure [CVE-2019-0804]">
<correction yorick-av "Réajustement des horodatages des images ; configuration de la taille du tampon VBV pour les fichiers MPEG1/2">
<correction zziplib "Correction d'accès non valable en mémoire [CVE-2018-6381], d'erreur de bus [CVE-2018-6540], de lecture hors limites [CVE-2018-7725], de plantage au moyen d'un fichier ZIP contrefait [CVE-2018-7726], de fuite de mémoire [CVE-2018-16548] ; rejet de fichier ZIP si la taille du répertoire central ou si le décalage du début répertoire central pointent au-delà de la fin du fichier ZIP [CVE-2018-6484, CVE-2018-6541, CVE-2018-6869]">
</table>

<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2018 4259 ruby2.3>
<dsa 2018 4332 ruby2.3>
<dsa 2018 4341 mariadb-10.1>
<dsa 2019 4373 coturn>
<dsa 2019 4374 qtbase-opensource-src>
<dsa 2019 4377 rssh>
<dsa 2019 4385 dovecot>
<dsa 2019 4387 openssh>
<dsa 2019 4388 mosquitto>
<dsa 2019 4389 libu2f-host>
<dsa 2019 4390 flatpak>
<dsa 2019 4391 firefox-esr>
<dsa 2019 4392 thunderbird>
<dsa 2019 4393 systemd>
<dsa 2019 4394 rdesktop>
<dsa 2019 4396 ansible>
<dsa 2019 4397 ldb>
<dsa 2019 4398 php7.0>
<dsa 2019 4399 ikiwiki>
<dsa 2019 4400 openssl1.0>
<dsa 2019 4401 wordpress>
<dsa 2019 4402 mumble>
<dsa 2019 4403 php7.0>
<dsa 2019 4405 openjpeg2>
<dsa 2019 4406 waagent>
<dsa 2019 4407 xmltooling>
<dsa 2019 4408 liblivemedia>
<dsa 2019 4409 neutron>
<dsa 2019 4410 openjdk-8>
<dsa 2019 4411 firefox-esr>
<dsa 2019 4412 drupal7>
<dsa 2019 4413 ntfs-3g>
<dsa 2019 4414 libapache2-mod-auth-mellon>
<dsa 2019 4415 passenger>
<dsa 2019 4416 wireshark>
<dsa 2019 4417 firefox-esr>
<dsa 2019 4418 dovecot>
<dsa 2019 4419 twig>
<dsa 2019 4420 thunderbird>
<dsa 2019 4422 apache2>
<dsa 2019 4423 putty>
<dsa 2019 4424 pdns>
<dsa 2019 4425 wget>
<dsa 2019 4426 tryton-server>
<dsa 2019 4427 samba>
<dsa 2019 4428 systemd>
<dsa 2019 4429 spip>
<dsa 2019 4430 wpa>
<dsa 2019 4431 libssh2>
<dsa 2019 4432 ghostscript>
<dsa 2019 4433 ruby2.3>
<dsa 2019 4434 drupal7>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction gcontactsync "Incompatible avec les dernières versions de firefox-esr">
<correction google-tasks-sync "Incompatible avec les dernières versions de firefox-esr">
<correction mozilla-gnome-kerying "Incompatible avec les dernières versions de firefox-esr">
<correction tbdialout "Incompatible avec les dernières versions de Thunderbird">
<correction timeline "Incompatible avec les dernières versions de Thunderbird">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.

