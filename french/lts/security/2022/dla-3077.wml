#use wml::debian::translation-check translation="bba716991bc7d1f5464a9230f22dbfd4ed96c0d2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un vulnérabilité de traversée potentielle de répertoires dans
ruby-tzinfo, une bibliothèque de gestion de fuseaux horaires pour le
langage de programmation Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31163">CVE-2022-31163</a>

<p>TZInfo est une bibliothèque Ruby qui fournit un accès aux données de
fuseau horaire et permet la conversion de données horaires en utilisant les
règles de fuseau horaire. Les versions antérieures à la version 0.36.1,
ainsi que celles antérieures à la version 1.2.10 quand elles sont utilisées
avec les sources de données de Ruby tzinfo-data, sont vulnérables à une
relative traversée de répertoires. Avec ces sources de données, les fuseaux
horaires sont définis dans des fichiers de Ruby. Il y a un fichier par
fuseau horaire. Ces fichiers sont chargés à la demande avec <q>require</q>.
Dans les versions affectées, <q>TZInfo::Timezone.get</q> échoue à valider
correctement les identifiants de fuseau horaire, autorisant un caractère de
saut de ligne dans l'identifiant. Avec la version 1.9.3 de Ruby et les
suivantes, <q>TZInfo::Timezone.get</q> peut être amené à charger des
fichiers inattendus avec <q>require</q>, les exécutant dans le processus de
Ruby. Les versions 0.3.61 et 1.2.10 incluent des correctifs pour valider
correctement les identifiants de fuseau horaire. Les versions 2.0.0 et les
suivantes ne sont pas vulnérables. La version 0.3.61 peut encore charger
des fichiers arbitraires à partir du chemin de chargement de Ruby si leur
nom suit les règles d'un identifiant de fuseau horaire valable et si le
fichier a un préfixe <q>tzinfo/definition</q> dans un répertoire dans le
chemin de chargement. Les applications devraient s'assurer que des fichiers
non fiables ne se trouvent pas dans un répertoire dans le chemin de
chargement. Pour contourner ce problème, l'identifiant de fuseau horaire
peut être validé avant d'être passé à <q>TZInfo::Timezone.get</q> en
s'assurant qu'il correspond à l'expression rationnelle
<q>\A[A-Za-z0-9+\-_]+(?:\/[A-Za-z0-9+\-_]+)*\z</q>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 1.2.5-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-tzinfo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3077.data"
# $Id: $
