#use wml::debian::translation-check translation="90d38c90a4f920296a242fd764ab2e3841c2f4e5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y a une attaque potentielle par déni de service (DoS) distant dans
XStream, une bibliothèque Java utilisée pour sérialiser des objets vers XML
et inversement.</p>

<p>Un attaquant peut avoir consommé 100 % des ressources du processeur,
mais la bibliothèque actuellement surveille et cumule le temps pris pour
ajouter des éléments aux collections, et envoie une exception si un seuil
fixé est dépassé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43859">CVE-2021-43859</a>

<p>XStream est une bibliothèque Java au code source libre utilisée pour
sérialiser des objets vers XML et inversement. Les versions antérieures à
1.4.19 peuvent permettre à un attaquant distant d'allouer 100 % du temps du
processeur sur le système cible selon le type de processeur ou une
exécution parallèle avec une charge utile telle que cela a pour conséquence
un déni de service uniquement par la manipulation du flux d'entrée traité.
XStream 1.4.19 surveille et cumule le temps pris pour ajouter des éléments
aux collections, et envoie une exception si un seuil fixé est dépassé. Il
est conseillé aux utilisateurs de mettre à niveau leur système dans les
plus brefs délais. Les utilisateurs qui ne peuvent pas effectuer de mise à
niveau peuvent définir le mode NO_REFERENCE pour éviter la récursion.
Consultez GHSA-rmr5-cpv2-vgjf pour davantage de détails sur un palliatif,
si une mise à niveau est impossible.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigé dans la
version 1.4.11.1-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2924.data"
# $Id: $
