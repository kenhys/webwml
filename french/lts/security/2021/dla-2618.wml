#use wml::debian::translation-check translation="73b0d2b593e4269d7fd2680dec45e73590350050" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans smarty3, un moteur de
patrons pour PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13982">CVE-2018-13982</a>

<p>Vulnérabilité de traversée de répertoires due à un nettoyage insuffisant de
code dans les patrons de Smarty. Cela permet à des attaquants contrôlant le
patron de Smarty de contourner les restrictions du répertoire fiable et de lire
des fichiers arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26119">CVE-2021-26119</a>

<p>Possibilité d’échappement du bac à sable parce qu’un accès
à $smarty.template_object est réalisable dans le mode bac à sable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26120">CVE-2021-26120</a>

<p>Vulnérabilité d’injection de code à l'aide d'un nom de fonction inattendu
après un {function name= sous-chaîne.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.1.31+20161214.1.c7d42e4+selfpack1-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets smarty3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de smarty3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/smarty3">\
https://security-tracker.debian.org/tracker/smarty3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2618.data"
# $Id: $
