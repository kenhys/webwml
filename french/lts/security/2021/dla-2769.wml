#use wml::debian::translation-check translation="88c3c8bab6cbc33888db63df198260d0689261de" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans XStream, une
bibliothèque Java pour sérialiser des objets en XML et les désérialiser.</p>

<p>Ces vulnérabilités peuvent permettre à un attaquant distant de charger et
exécuter du code arbitraire d’un hôte distant simplement en manipulant le flux
d’entrée traité.</p>

<p>XStream définit désormais une liste blanche par défaut, c’est-à-dire que
toutes les classes sont bloquées sauf les types pour lesquels des convertisseurs
sont explicitement déclarés. Habituellement une liste noire par défaut existait,
c’est-à-dire que toutes les classes critiques actuellement connues de
l’environnement d’exécution de Java étaient bloquées. La principale raison de
la liste noire était la compatibilité qui permettait d’utiliser les nouvelles
versions d’XStream comme substitution. Toutefois, cette approche a échoué.
Une liste grandissante de rapports de sécurité a démontré qu’une liste noire
est intrinsèquement peu sûre, sans tenir compte du fait que les types de
bibliothèques tierces n’étaient même pas envisagés. Un scénario de liste noire
doit être évité, car il procure un faux sentiment de sécurité.</p>

<p>Consultez aussi <a rel="nofollow" href="https://x-stream.github.io/security.html#framework">https://x-stream.github.io/security.html#framework</a>.</p>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.4.11.1-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2769.data"
# $Id: $
