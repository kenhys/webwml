#use wml::debian::translation-check translation="543dbaa4d9a3bfb77c9dc202f09c9ae6e64437a9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Powerline Gitstatus, un greffon de barre d’état pour l’éditeur VIM,
permettait l’exécution de code arbitraire. Les dépôts Git peuvent contenir une
configuration par dépôt changeant le comportement de git, dont l’exécution de
commandes arbitraires. Lors de l’utilisation de powerline-gitstatus, le
changement de répertoire exécute des commandes de git afin d’afficher des
informations sur le dépôt utilisé dans le prompt. Si un attaquant pouvait
persuader un utilisateur de changer de dépôt pour un contrôlé par cet attaquant,
tel qu’un système de fichiers partagé ou une archive extraite,
powerline-gitstatus pouvait exécuter des commandes arbitraires contrôlées par
l’attaquant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.3.2-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets powerline-gitstatus.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de powerline-gitstatus,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/powerline-gitstatus">\
https://security-tracker.debian.org/tracker/powerline-gitstatus</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3277.data"
# $Id: $
