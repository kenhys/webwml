#use wml::debian::translation-check translation="2388531cb418a0b2ed18b0f34df9279b0bae460a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Squid, un serveur mandataire et de cache de haute performance pour des
clients web, était affecté par les vulnérabilités de sécurité suivantes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12526">CVE-2019-12526</a>

<p>Le traitement de réponse à un URN dans Squid souffrait d’un dépassement de
tampon basé sur le tas. Lors de la réception de données d’un serveur distant
en réponse à une requête d’URN, Squid échouait à assurer que la réponse pouvait
être contenue dans le tampon. Cela conduisait à des données contrôlées par
l’attaquant débordant du tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18677">CVE-2019-18677</a>

<p>Lorsque le réglage append_domain est utilisé (dû aux caractères ajoutés
n’interagissant pas correctement avec les restrictions de longueur de nom
d’hôte), il pouvait de manière inappropriée rediriger le trafic vers des sources
non prévues. Cela se produit à cause du traitement incorrect du message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18678">CVE-2019-18678</a>

<p>Une erreur de programmation permet à des attaquants de transférer
illégalement des requêtes HTTP au travers du logiciel d’interface vers une
instance Squid qui fractionne la tuyauterie des requêtes HTTP différemment,
aboutissant à des caches corrompus de réponse de message (entre un client et
Squid) à l’aide de contenu contrôlé par l’attaquant dans des URL arbitraires.
Les effets sont isolés du logiciel entre le client de l’attaquant et Squid.
Il n’y a pas d’effets sur Squid lui-même, ni sur aucun serveur amont. Ce
problème concerne un en-tête de requête contenant une espace entre un nom
d’en-tête et un deux-points.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18679">CVE-2019-18679</a>

<p>Dû à une gestion incorrecte de données, Squid est sujet à une divulgation
d'informations lors du traitement de « Digest Authentication » HTTP.
Des jetons créés pour l’occasion contiennent la valeur brute d’octets d’un
pointeur se trouvant dans l’allocation de mémoire de tas. Cette information
réduit les protections ASLR et peut aider des attaquants à isoler des zones de
mémoire à exploiter pour des attaques d’exécution de code à distance.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.8-6+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2028.data"
# $Id: $
