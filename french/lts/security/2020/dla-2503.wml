#use wml::debian::translation-check translation="bd26538874c6fd699be2cc566da3934fb8d9bbb6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans node-ini, un analyseur
pour le format .ini et un sérialiseur pour Node.js, avec lequel une application
pourrait être exploitée à l’aide d’un fichier d’entrée malveillant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7788">CVE-2020-7788</a>

<p>Cela affecte le paquet ini avant sa version 1.3.6. Si un attaquant soumettait
un fichier INI malveillant à une application qui l’analyse avec ini.parse, le
prototype de l’application serait corrompu. Cela pourrait être exploité ensuite
selon le contexte.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.1.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-ini.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2503.data"
# $Id: $
