#use wml::debian::translation-check translation="1e18672148aa01ee6fccb5fc4a16173d49093f54" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

<p>Un problème a été découvert dans la bibliothèque complémentaire de
chiffrement de Go, c'est-à-dire, golang-googlecode-go-crypto. Si plus de 256 GiB
de séquences de clés sont générés ou si le compteur dépasse 32 bits,
l’implémentation d’amd64 générera d’abord une sortie incorrecte, puis reviendra
à la séquence précédente. Des répétitions d’octets de séquence peuvent conduire
à une perte de confidentialité dans les applications de chiffrement ou à une
prédictibilité dans les applications CSPRNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11841">CVE-2019-11841</a>

<p>Un problème de contrefaçon de message a été découvert dans
crypto/openpgp/clearsign/clearsign.go dans les bibliothèques complémentaires de
chiffrement de Go. L’en-tête <q>Hash</q> d’Armor précise le(s) algorithme(s) de
hachage cryptographique utilisé(s) pour les signatures. Étant donné que la
bibliothèque omet en général l’analyse de l’en-tête d’Armor, un attaquant peut
non seulement embarquer des en-têtes arbitraires d’Armor, mais aussi ajouter en
début du texte arbitraire aux messages en clair sans invalider les signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

<p>La fonction golang.org/x/crypto permet un plantage lors de la vérification de
signature dans le paquet golang.org/x/crypto/ssh. Un client peut attaquer un
serveur SSH acceptant des clés publiques. De plus, un serveur peut attaquer
n’importe quel client SSH.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:0.0~git20170407.0.55a552f+REALLY.0.0~git20161012.0.5f31782-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-go.crypto.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-go.crypto, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-go.crypto">https://security-tracker.debian.org/tracker/golang-go.crypto</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2402.data"
# $Id: $
