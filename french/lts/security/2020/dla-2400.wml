#use wml::debian::translation-check translation="39423bf54bdfb85ad82c74e1c774db8b370fc735" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Apache ActiveMQ, un courtier de messages en Java, utilise
LocateRegistry.createRegistry() pour créer le registre RMI JMX et lier le
serveur à l’entrée <q>jmxrmi</q>. Il est possible de se connecter au registre
sans authentification et appeler la méthode <q>rebind</q> pour relier jmxrmi
à quelque chose d’autre. Si un attaquant crée un autre serveur pour servir de
mandataire à l’originale, et réalise la liaison, il devient en pratique un homme
du milieu et peut intercepter les accréditations lors de la connexion d’un
 utilisateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 5.14.3-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets activemq.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de activemq, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/activemq">https://security-tracker.debian.org/tracker/activemq</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2400.data"
# $Id: $
