#use wml::debian::translation-check translation="ca21494ab8950793a16502f84cc7f0343dad7e83" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la boîte à outils antivirus
ClamAV.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3327">CVE-2020-3327</a>

<p>Une lecture hors limites dans le module d’analyse d’archives ARJ pouvait
provoquer un déni de service. Le correctif dans la version 0.102.3 était
incomplet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3350">CVE-2020-3350</a>

<p>Un utilisateur malveillant pouvait amener clamscan, clamdscan ou clamonacc
à déplacer ou supprimer un fichier différent de celui attendu quand ceux-ci
étaient utilisés avec une des options --move ou --remove. Cela pouvait être
utilisé pour supprimer des fichiers spéciaux du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3481">CVE-2020-3481</a>

<p>Le module d’archive EGG était vulnérable à un déni de service à l’aide d’un
déréférencement de pointeur NULL dû à une gestion incorrecte d’erreur. La base
de données officielle de signatures échappait à ce problème car les signatures
présentes évitaient l’utilisation de l’analyseur d’archive EGG.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.102.4+dfsg-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de clamav, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/clamav">https://security-tracker.debian.org/tracker/clamav</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2314.data"
# $Id: $
