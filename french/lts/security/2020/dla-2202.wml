#use wml::debian::translation-check translation="bb3f804000185056dc659c88709d05facf94aa3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Ansible, un système de
gestion de configuration, déploiement et exécution de tâches.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14846">CVE-2019-14846</a>

<p>Ansible journalisait au niveau DEBUG, ce qui conduisait à une divulgation
d’accréditations si un greffon utilisait une bibliothèque qui journalisait les
accréditations au niveau DEBUG. Ce défaut n’affecte pas les modules Ansible,
comme ceux exécutés dans un processus distinct.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1733">CVE-2020-1733</a>

<p>Un défaut de situation de compétition a été découvert lors de l’utilisation
d’un « playbook » avec un « become user » sans droits. Lorsqu’Ansible avait
besoin d’exécuter un module avec « become user », le répertoire temporaire était
créé dans /var/tmp. Ce répertoire était créé avec « umask 77 &amp;&amp; mkdir -p dir ».
Cette opération n’échouait pas si le répertoire existait déjà et appartenait
à un autre utilisateur. Un attaquant pourrait exploiter cela pour contrôler le
« become user » car le répertoire cible pouvait être retrouvé par itération sur
« /proc/pid/cmdlinez ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1739">CVE-2020-1739</a>

<p>Un défaut a été découvert lorsque le mot de passe est réglé avec l’argument
<q>password</q> du module svn, utilisé sur la ligne de commande de svn,
l’exposant aux autres utilisateurs appartenant au même nœud. Un attaquant
pourrait exploiter cela en lisant le fichier de ligne de commande à partir de ce
PID particulier du procfs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1740">CVE-2020-1740</a>

<p>Un défaut a été découvert lors de l’utilisation d’Ansible Vault pour
l’édition de fichiers chiffrés. Quand un utilisateur exécute « ansible-vault
edit », un autre utilisateur sur le même ordinateur peut lire l’ancienne et la
nouvelle phrase secrète car elle est créée dans un fichier temporaire avec
mkstemp et le descripteur de fichier renvoyé est fermé et la méthode write_data
est appelée pour écrire la phrase secrète dans le fichier. Cette méthode détruit
le fichier avant de le recréer de manière insécurisée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.2+dfsg-2+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ansible.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2202.data"
# $Id: $
