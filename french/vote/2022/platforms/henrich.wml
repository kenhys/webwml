#use wml::debian::template title="Plateforme de Hideki Yamane" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="28bd8ae61078a0008a381c3b670b2afc016e5af1" maintainer="Jean-Philippe Mengual"

<p>Tremblements de terre, Pandemie, Guerre… (<em>sigh</em>) Bon la vie est courte, vraiment.
Il me semble que je peux candidater, alors allons-y. Tentons le coup.</p>
<h2 id="self-introduction">Ma présentation</h2>
<p>Il se peut que certains me connaissent, mais la plupart d'entre vous non.
Je me permets donc de me présenter un peu.</p>
<p>Je m'appelle Hideki Yamane (やまねひでき), la quarantaine/cinquantaine, et je vis
au Japon. Comme indiqué sur nm.debian.org, <a href="https://nm.debian.org/person/henrich/">je 
suis devenu développeur Debian en 2010</a> (comme le temps passe
vite)&nbsp;: mon nom de compte est « henrich ».</p>
<p>Qu'ai-je fait dans le passé&nbsp;?:</p>
<ul>
<li><a href="https://qa.debian.org/developer.php?login=henrich%40debian.org&amp;comaint=yes">j'ai assuré
la maintenance de quelques paquets</a> (mais paresseusement, désolé)</li>
<li>j'ai traduit (anglais -&gt; japonais) quelques
<a href="https://www.debian.org/releases/stable/amd64/release-notes/index.ja.html">notes de publication</a>
(Debian 6 à 11), <a href="https://www.debian.org/doc/manuals/developers-reference/index.ja.html">la référence du développeur</a>, etc.</li>
<li>j'ai coordonné
 <a href="https://www.debian.org/News/2015/20150206">la mise en place
d'un miroir security.debian.org au Japon</a> (merci les DSAs et
<a href="https://www.sakura.ad.jp/en/corporate/">Sakura Internet</a>)</li>
<li>j'ai poussé pour convertir 
<a href="https://www.debian.org/doc/debian-policy/">la Charte
Debian</a> en <a href="https://tracker.debian.org/news/864773/accepted-debian-policy-4100-all-source-into-unstable/">reStructuredText 
avec sphinx</a> à partir de DocBook (merci Russ, Sean et les autres)</li>
<li>j'ai poussé pour 
<a href="https://wiki.debian.org/HidekiYamane/material?action=AttachFile&amp;do=view&amp;target=Lets_shrink_repo.pdf">utiliser
la compression XZ dans les fichiers .deb</a> à la DebConf12</li>
<li>en tant que rédacteur technique, j'ai écrit (mensuellement puis, plus tard)
bimensuellement) les séries
 “<a href="https://gihyo.jp/magazine/SD/">Debian Hot Topics</a>” de
2013 à 2021 (en japonais)</li>
<li>j'ai organisé quelques événements locaux au Japon, en particulier à
Tokyo.</li>
<li>etc.</li>
</ul>
<h3 id="warning-warning">ATTENTION&nbsp;! ATTENTION&nbsp;!</h3>
<p>Voilà pourquoi je suis le “<strong>pire</strong>” candidat au poste de responsable du projet&nbsp;:</p>
<ul>
<li>Je ne suis pas bon en anglais (je peux utiliser du yaourt anglophone
;), donc je ne pourrai pas répondre à des questions compliquées que
certains affectionnent.</li>
<li>Je n'ai aucune compétence en gestion de conflits entre nos contributeurs.
Restez calmes, sympa et en sécurité.</li>
<li>Cela veut dire que les contributeurs Debian (oui,
« “<strong>VOUS</strong> ») devront beaucoup m'aider (beaucoup plus que les
autres candidats).</li>
</ul>
<h2 id="motivation">Motivation</h2>
<p>Alors peut-être que vous voulez savoir ce que je souhaite faire pendant
mon mandat de responsable du projet. Eh bien voilà.</p>
<ul>
<li>Apporter une meilleure « expérience » Debian à nos contributeurs et à nos
utilisateurs.</li>
</ul>
<p><a href="https://lists.debian.org/stats/debian-devel-changes.png">Nous
développons beaucoup Debian jour après jour</a>, mais il me semble que cela 
ne parvient pas à la majorité de nos utilisateurs. Je ne suis pas sûr de
la manière d'améliorer les choses (faciliter l'accès à testing/unstable
dans l'installeur de Debian&nbsp;? mettre à jour stable plus souvent&nbsp;?), 
mais ce serait bien d'y arriver.</p>
<ul>
<li>Donner plus d'écho (du moins essayer) aux voix de nos utilisateurs et
les écouter avant de discuter entre contributeurs.</li>
</ul>
<p>Davantage essayer, échouer et progresser grâce à cela. Nous sommes dans
les années 2020. <a href="https://agilemanifesto.org/">Soyons Agile</a>.
Bon OK, ce n'est pas facile, mais cela vaut le coup d'essayer, vu que les « règles »
ne sont pas des lois physiques édictées par un dieu identifié, mais simplement
des choses qu'on a décidées par le passé.</p>
<ul>
<li>Davantage de changements dans l'infrastructure (cela peut demander
plus de dons et d'efforts)</li>
</ul>
<p>OK, pour essayer ce qui précède, il se peut qu'il faille plus de changements
dans nos infrastructures - Web, Wiki, système de suivi des bogues, dépôt,
etc. Grâce aux administrateurs système de Debian et aux autres contributeurs,
nous sommes heureux de notre développement actuel. Mais le faire
simplement fonctionner n'est pas suffisant - Je veux « l'accroître »
pour en faire une fusée pour la lune :)</p>
<p>Et que cette fusée donne l'environnement le plus confortable
possible aux développeurs, plus de stabilité et moins de vulnérabilités
aux administrateurs, un environnement de bureau raisonnablement à jour pour
nos utilisateurs courants, plus d'applications et de documentations traduites
pour les gens qui ne parlent pas anglais, etc.</p>
<ul>
<li>Renouveler et transmettre la connaissance et les techniques</li>
</ul>
<p>Pour changer les choses, il faut savoir que détruire, c'est facile.
Alors discutons, transmettons et améliorons-nous avec de nouveaux contributeurs.</p>
<p>Au Japon, l'immense temple Shinto est renouvelé tous les vingt ans, si bien qu'on
l'appelle le “式年遷宮” (Shikinen Sengu). Il garde la connaissance et le savoir-faire
(comment créer le temple Shinto à partir de rien) issus des maîtres
qui transmettent aux nouveaux ingénieurs. Ainsi, les plus vieux temples
Shinto ont été construits en bois et ont été maintenus pendant plus de 1 000 ans.</p>
<p>Attirer de nouveaux contributeurs et demander aux administrateurs de transmettre
nos connaissances actuelles pour les prochaines décennies.</p>
<h2 id="overall">Résumé</h2>
<p>Nous avons fait de bonnes choses et nous pouvons faire un meilleur travail
si nous ne craignons pas les changements.</p>
<p>Bousculons-nous un peu, essayons.</p>
