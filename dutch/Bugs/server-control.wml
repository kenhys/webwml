#use wml::debian::template title="Debian server voor BTS&mdash;beheer" NOHEADER=yes NOCOPYRIGHT=true
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#use wml::debian::translation-check translation="d11193e66dd678156a1f8aa4c02b5ec7ae7eb116"

<h1>Inleiding tot de mailserver voor het beheren en manipuleren van bugs</h1>

<p>
Net zoals <code>request@bugs.debian.org</code> het toelaat om <a
href="server-request">gegevens en documentatie over bugs op te vragen via
e-mail</a>, maakt <code>control@bugs.debian.org</code> het mogelijk om
bugrapporten op verschillende manieren te behandelen.
</p>

<p>
De beheerserver werkt net als de verzoekserver, behalve dat er enkele
extra commando's mogelijk zijn. In feite betreft het hetzelfde programma.
De twee adressen zijn enkel onderling gescheiden om te voorkomen dat gebruikers
fouten zouden maken en problemen zouden veroorzaken, terwijl ze alleen maar
proberen om informatie op te vragen.
</p>

<p>
Aangezien de commando's die specifiek zijn voor de beheerserver in feite
de toestand van een bug wijzigen, wordt een melding over de verwerking van de
commando's gestuurd naar de beheerder van het(de) pakket(ten) waaraan
de gewijzigde bugs toegewezen zijn. Daarnaast worden de naar de server
gestuurde e-mail en de daaruit voortvloeiende wijzigingen gelogd in het
bugrapport, waardoor deze beschikbaar zijn in de WWW-pagina's.
</p>

<p>
Voor details over de basisprincipes van de werking van de mailservers en de
gemeenschappelijke commando's die beschikbaar zijn bij het versturen van een
e-mail naar een van beide adressen, kunt u te rade gaan bij de
<a href="server-request#introduction">inleiding tot de verzoekserver</a>,
die u kunt vinden op het World Wide Web in het bestand
<code>bug-log-mailserver.txt</code>, of u kunt
<code>help</code> sturen naar een van beide mailservers.
</p>

<p>
De <a href="server-refcard">referentiekaart</a> voor de
mailservers is beschikbaar op het WWW, in
<code>bug-mailserver-refcard.txt</code> of per e-mail met het commando
<code>refcard</code>.
</p>


<h1>Op de beheermailserver beschikbare commando's</h1>

  <table style="margin-left:auto;margin-right:auto">
    <tr>
    <td align="center">Algemeen</td>
    <td align="center">Versiebeheer</td>
    <td align="center">Duplicaten</td>
    <td align="center">Varia</td>
    </tr>
    <tr>
      <!-- General -->
      <td valign="top">
	<ul class="nodecoration">
	  <li><a href="#reassign">reassign</a></li>
	  <li><a href="#severity">severity</a></li>
	  <li><a href="#tag">tags</a></li>
	  <li><a href="#retitle">retitle</a></li>
	  <li><a href="#submitter">submitter</a></li>
	  <li><a href="#affects">affects</a></li>
	  <li><a href="#summary">summary</a></li>
	  <li><a href="#outlook">outlook</a></li>
	</ul>
      </td>
      <!-- Versioning -->
      <td valign="top">
	<ul class="nodecoration">
	  <li><a href="#found">found</a> | <a href="#notfound">notfound</a></li>
	  <li><a href="#fixed">fixed</a> | <a href="#notfixed">notfixed</a></li>
	  <li><a href="#reopen">reopen</a></li>
	  <!-- <dt>(close)</dt> Deprecated -->
	</ul>
      </td>
      <!-- Duplicates -->
      <td valign="top">
	<ul class="nodecoration">
	  <li><a href="#merge">merge</a> | <a href="#unmerge">unmerge</a></li>
	  <li><a href="#forcemerge">forcemerge</a></li>
	  <li><a href="#clone">clone</a></li>
	</ul>
      </td>
      <!-- Misc. -->
      <td valign="top">
	<ul class="nodecoration">
	  <li><a href="#thanks">thanks</a></li>
	  <li><a href="#comment">#</a></li>
	  <li><a href="#forwarded">forwarded</a> |  <a href="#notforwarded">notforwarded</a></li>
	  <li><a href="#owner">owner</a> | <a href="#noowner">noowner</a></li>
	  <li><a href="#block">block</a> | <a href="#unblock">unblock</a></li>
	  <li><a href="#archive">archive</a> | <a href="#unarchive">unarchive</a></li>
	  <li><a href="server-request#user">user</a> |
	    <a href="server-request#usertag">usertag</a> |
	    <a href="server-request#usercategory">usercategory</a></li>
	</ul>
      </td>
    </tr>
  </table>

<dl>
  <dt><a name="reassign"><code>reassign</code> <var>bugnummer</var>
    <var>pakket</var> [ <var>versie</var> ]</a></dt>
  <dd>
    <p>
    Registreert dat bug #<var>bugnummer</var> een bug is in <var>pakket</var>.
    Dit kan gebruikt worden om het pakket in te stellen indien de gebruiker
    de pseudo-kop vergeten was, of om een eerdere toewijzing te wijzigen.
    Er worden niemand meldingen gestuurd (behalve de gebruikelijke
    informatie in het verwerkingstranscript).
    </p>

    <p>
    Indien u een <var>versie</var> opgeeft, zal het bugvolgsysteem noteren
    dat de bug die versie van het pakket waaraan de bug opnieuw
    toegewezen werd, aantast.
    </p>
    <p>
      U kunt ineens een bug toewijzen aan twee pakketten door de pakketnamen
      te scheiden met een komma. U zou dit <em>echter enkel</em> mogen doen
      als een aanpassing aan <em>gelijk welk van beide</em> pakketten de bug
      kan repareren. Is dit niet het geval, dan zou u de bug moeten
      <a href="#clone">klonen</a> (commando clone) en de kloon opnieuw moeten
      toewijzen aan dat andere pakket.
    </p>
  </dd>


  <dt><a name="reopen"><code>reopen</code> <var>bugnummer</var>
   [ <var>opsteller-adres</var> | <code>=</code> | <code>!</code> ]</a></dt>
  <dd>
    <p>
    Heropent #<var>bugnummer</var> en bevrijdt alle gerepareerde versies
    wanneer het gesloten wordt.
    </p>

    <p>
    Standaard, of indien u <code>=</code> opgeeft, blijft de originele indiener
    de opsteller van het rapport, zodat deze een bevestiging ontvangt wanneer
    de bug opnieuw gesloten wordt.
    </p>

    <p>
    Indien u een <var>opsteller-adres</var> opgeeft, zal de opsteller ingesteld
    worden op het adres dat u opgeeft. Indien u zelf de nieuwe opsteller wilt
    worden van het heropende rapport, kunt u de snelnotatie <code>!</code>
    gebruiken of uw eigen e-mailadres opgeven.
    </p>

    <p>
    Gewoonlijk is het een goede zaak om de persoon die geregistreerd zal worden
    als de rapportopsteller, erover in te lichten dat u het rapport zult
    heropenen, zodat deze op de hoogte is dat er een bevestiging verwacht mag
    worden wanneer het rapport opnieuw gesloten wordt.
    </p>

    <p>
    Indien de bug niet gesloten is, zal een reopen-opdracht geen enkel effect
    hebben, zelfs geen wijziging van de originele opsteller. Om de opsteller
    van een openstaand bugrapport te veranderen, moet u het commando
    <code>submitter</code> gebruiken. Merk op dat de originele indiener over
    deze wijziging geïnformeerd zal worden.
    </p>

    <p>
    Indien geregistreerd werd dat de bug voor een specifieke versie van een
    pakket gesloten werd, maar die bug in een latere versie terug opduikt, kunt
    u beter het commando <code>found</code> gebruiken.
    </p>
  </dd>


  <dt><a name="found"><code>found</code> <var>bugnummer</var> [
    <var>versie</var> ]</a></dt>
  <dd>
    <p>
    Registreren dat #<var>bugnummer</var> aangetroffen werd in de vermelde
    <var>versie</var> van het pakket waaraan de bug toegewezen is.
    <var>versie</var> kan een volledig uniek versienummer zijn,
    volgens de vorm <var>bronpakketnaam/versie</var>.
    </p>

    <p>
    Het bugvolgsysteem gebruikt deze informatie, in combinatie met de bij het
    sluiten van bugs geregistreerde gerepareerde versies, om lijsten met bugs
    weer te geven die in verschillende versies van elk pakket open staan. Het
    systeem beschouwt een bug als openstaand als geen versies
    gerepareerd zijn, of wanneer het aantreffen van deze bug recenter gebeurde
    dan het repareren ervan.
    </p>

    <p>
    Indien geen <var>versie</var> opgegeven werd, dan wordt voor deze bug de
    lijst met gerepareerde versies gewist. Dit is identiek aan het gedrag van
    <code>reopen</code>.
    <var>versie</var> kan een volledig uniek versienummer zijn,
    volgens de vorm <var>bronpakketnaam/versie</var>.
    </p>

    <p>
    Dit commando heeft tot effect dat een bug enkel gemarkeerd wordt als niet
    gerepareerd als er geen versienummer opgegeven wordt, of als het nummer van
    de <var>versie</var> die gemarkeerd staat als <var>versie</var> waarin de
    bug aangetroffen werd, even groot of groter is dan de grootste
    <var>versie</var> die als gerepareerd gemarkeerd staat. (Indien u er zeker
    van bent dat u de bug als niet gerepareerd wilt markeren, gebruik dan het
    commando <code>reopen</code> in combinatie met het commando
    <code>found</code>.)
    </p>

    <p>
    Dit commando werd ingevoerd omdat het de voorkeur geniet boven
    <code>reopen</code>, omdat het moeilijk was om aan de syntaxis van dat
    commando een <var>versie</var> toe te voegen zonder aan dubbelzinnigheid te
    lijden.
    </p>
  </dd>


  <dt><a name="notfound"><code>notfound</code> <var>bugnummer</var>
    <var>versie</var></a></dt>
  <dd>
    <p>
    Verwijderen van de registratie dat #<var>bugnummer</var> aangetroffen werd
    in de vermelde <var>versie</var> van het pakket waaraan de bug toegewezen
    is.
    <var>versie</var> kan een volledig uniek versienummer zijn,
    volgens de vorm <var>bronpakketnaam/versie</var>.
    </p>

    <p>
    Dit verschilt van het sluiten van de bug bij die versie doordat de bug ook
    niet als opgelost wordt vermeld voor die versie. Er zal over die versie geen
    informatie bekend zijn. Dit is bedoeld voor het herstellen van fouten in de
    registratie van wanneer een bug gevonden werd.
    </p>
  </dd>


  <dt><a name="fixed"><code>fixed</code> <var>bugnummer</var>
    <var>versie</var></a></dt>
  <dd>
    <p>
    Aangeven dat bug #<var>bugnummer</var> opgelost werd in de vermelde
    <var>versie</var> van het pakket waaraan de bug toegewezen werd.
    <var>versie</var> kan een volledig uniek versienummer zijn,
    volgens de vorm <var>bronpakketnaam/versie</var>.
    </p>

    <p>
    Dit zorgt er <em>niet</em> voor dat de bug als gesloten gemarkeerd wordt,
    het voegt alleen maar een andere versie toe waarin de bug is gerepareerd.
    Gebruik het adres bugnummer-done om een bug te sluiten en deze als opgelost
    te markeren in een specifieke versie.
    </p>
  </dd>


  <dt><a name="notfixed"><code>notfixed</code> <var>bugnummer</var>
    <var>versie</var></a></dt>
  <dd>
    <p>
    Verwijderen van de registratie dat bug #<var>bugnummer</var> opgelost is
    in de vermelde <var>versie</var>.
    <var>versie</var> kan een volledig uniek versienummer zijn,
    volgens de vorm <var>bronpakketnaam/versie</var>.
    </p>

    <p>
    Dit commando is het equivalent van <code>found</code> gevolgd door
    <code>notfound</code> (de registratie found verwijdert de registratie
    fixed voor een specifieke versie en de registratie notfound verwijdert de
    registratie found) met uitzondering van het feit dat de bug niet
    heropend wordt als de versie waarin ze als gevonden wordt
    geregistreerd groter is dan elke bestaande gerepareerde versie. Dit
    is bedoeld voor het herstellen van fouten in de registratie van wanneer
    een bug opgelost werd; in de meeste gevallen gebruikt u eigenlijk
    beter <code>found</code> en niet <code>notfixed</code>.
    </p>
  </dd>


  <dt><a name="submitter"><code>submitter</code> <var>bugnummer</var>
    <var>adres-v/d-indiener</var> | <code>!</code></a></dt>
  <dd>
    <p>
    Wijzigt de opsteller van #<var>bugnummer</var> naar
    <var>adres-v/d-indiener</var>.
    </p>

    <p>
    Indien u de nieuwe opsteller van het rapport wilt worden, kunt u
    de snelnotatie <code>!</code> gebruiken of uw eigen e-mailadres opgeven.
    </p>

    <p>
    Daar waar het commando <code>reopen</code> de indiener aanpast van de andere
    bugs die samengevoegd werden met de heropende bug,
    heeft <code>submitter</code> geen effect op samengevoegde bugs.
    </p>
  </dd>


  <dt><a name="forwarded"><code>forwarded</code> <var>bugnummer</var>
    <var>adres</var></a></dt>
  <dd>
    <p>
    Vermeldt dat <var>bugnummer</var> doorgestuurd werd naar de bovenstroomse
    beheerder op <var>adres</var>. Dit stuurt het rapport zelf niet door.
    Dit kan gebruikt worden om een bestaand foutief doorgestuurd-naar-adres
    aan te passen, of om een nieuwe registratie in te voeren voor een bug
    die niet eerder als doorgestuurd werd vermeld. In het algemeen moet
    <var>adres</var> een URI zijn, of mogelijk een e-mailadres. Door waar
    mogelijk een URI te gebruiken, kan gereedschap een extern
    bugvolgsysteem (zoals bugzilla) vragen naar de toestand van een bug.
    </p>

    <p>
      Gebruiksvoorbeeld:
    </p>

    <pre>
      forwarded 12345 http://bugz.illa.foo/cgi/54321
    </pre>
  </dd>


  <dt><a name="notforwarded"><code>notforwarded</code>
    <var>bugnummer</var></a></dt>
  <dd>
    <p>
    Doet elk idee dat <var>bugnummer</var> doorgestuurd werd naar een
    bovenstroomse beheerder, vergeten. Indien de bug niet als doorgestuurd
    geregistreerd stond, heeft dit geen enkel effect.
    </p>
  </dd>


  <dt><a name="retitle"><code>retitle</code> <var>bugnummer</var>
    <var>nieuwe-titel</var></a></dt>
  <dd>
    <p>
    Verandert de titel van een bugrapport naar die welke opgegeven wordt
    (standaard is dat de inhoud van het <code>Onderwerp</code>-kopveld
    van het e-mailbericht met het originele rapport). Dit zal eveneens
    de titel aanpassen van alle bugrapporten waarmee deze bug samengevoegd werd.
    </p>
  </dd>


  <dt><a name="severity"><code>severity</code> <var>bugnummer</var>
    <var>ernstigheidsgraad</var></a></dt>
  <dd>
    <p>
    Stelt het ernstigheidsniveau van bugrapport #<var>bugnummer</var> in op
    <var>ernstigheidsgraad</var>. Er wordt geen kennisgeving gestuurd naar
    de gebruiker die de bug rapporteerde.
    </p>

    <p>
    Ernstigheidsgraden zijn: <bts_severities>.
    </p>

    <p>
    Voor <a href="Developer#severities">hun betekenis</a> kunt u de
    algemene documentatie voor ontwikkelaars over het bugsysteem raadplegen.
    </p>
  </dd>

  <dt><a name="affects"><code>affects</code> <var>bugnummer</var>
      [ <code>+</code> | <code>-</code> | <code>=</code>
      ] <var>pakket</var> [ <var>pakket</var> ... ]</a></dt>
  <dd>
    <p>
      Geeft aan dat een bug een ander pakket aantast. In het geval
      waarin <var>bugnummer</var> <var>pakket</var> defect maakt,
      ook al is de bug eigenlijk aanwezig in het pakket waaraan de bug
      toegewezen is, zorgt dit ervoor dat de bug standaard vermeld wordt in
      de buglijst van <var>pakket</var>. In het algemeen zou dit gebruikt
      moeten worden voor gevallen waarbij de bug voldoende ernstig is om
      ertoe te leiden dat meerdere bugrapporten van gebruikers
      toegewezen worden aan het verkeerde pakket. <code>=</code> stelt
      in dat de bug de opgegeven lijst pakketten aantast en
      is de standaardactie als geen pakketten opgegeven worden;
      <code>-</code> verwijdert de opgegeven pakketten uit de lijst
      van aangetaste pakketten; <code>+</code> voegt de
      opgegeven pakketten toe aan de lijst van aangepaste pakketten
      en is de standaardactie als pakketten opgegeven worden.
    </p>
  </dd>

  <dt><a name="summary"><code>summary</code> <var>bugnummer</var>
      [<var>berichtnummer</var> | <var>samenvattende-tekst</var>]</a></dt>
  <dd>
    <p>
      Selecteert een bericht om als bugsamenvatting te gebruiken. De eerste
      paragraaf van dat bericht welke geen pseudo-kopveld of geen stuurcommando
      is, wordt ontleed en wordt ingesteld als de samenvatting van de bug en
      weergegeven bovenaan de bugrapportpagina. Dit is nuttig in gevallen
      waarin het originele rapport het probleem niet correct beschrijft of
      wanneer de bug veel berichten telt, waardoor het moeilijk wordt om het
      eigenlijke probleem te identificeren.
    </p>
    <p>
      Indien <var>berichtnummer</var> niet vermeldt wordt, wist dit de
      samenvatting. <var>berichtnummer</var> is het nummer van het bericht,
      zoals dat vermeld wordt in de uitvoer van het bugrapport-cgi-script;
      indien het <var>berichtnummer</var> 0 opgegeven werd, wordt het huidige
      bericht gebruikt (dit betekent het aan control@bugs.debian.org gezonden
      bericht dat het stuurcommando summary bevat).
    </p>
    <p>
      Indien <var>berichtnummer</var> geen numeriek getal is en niet leeg is,
      wordt aangenomen dat dit de tekst is die als samenvatting moet worden
      ingesteld.
    </p>
  </dd>

  <dt><a name="outlook"><code>outlook</code> <var>bugnummer</var>
      [<var>berichtnummer</var> | <var>vooruitzicht-tekst</var>]</a></dt>
  <dd>
    <p>
      Selecteert een bericht om te gebruiken als vooruitzicht voor het oplossen
      van een bug (of de huidige toestand van het oplossen van een bug). De
      eerste paragraaf van dat bericht welke geen pseudo-koptekst of geen
      stuurcode is, wordt ontleed en ingesteld als vooruitzicht voor die bug en
      wordt getoond bovenaan de bugrapportpagina. Dit is nuttig voor de
      coördinatie met anderen die werken aan het oplossen van deze bug
      (bijvoorbeeld bij een bug squashing party).
    </p>
    <p>
      Indien geen <var>berichtnummer</var> opgegeven wordt, wordt het
      vooruitzicht gewist. <var>berichtnummer</var> is het nummer van het
      bericht zoals dat vermeld wordt in de uitvoer van het
      bugrapport-cgi-script; als 0 opgegeven wordt als <var>berichtnummer</var>,
      dan wordt het huidige bericht gebruikt (dit is het bericht dat gestuurd
      werd naar control@bugs.debian.org en het stuurcommando outlook bevat).
    </p>
    <p>
      Indien <var>berichtnummer</var> geen numeriek getal is en niet leeg is,
      wordt aangenomen dat dit de tekst is die als vooruitzicht moet worden
      ingesteld.
    </p>
  </dd>


  <dt><a name="clone"><code>clone</code> <var>bugnummer</var> <var>NieuwID</var>
    [ <var>nieuwe ID's</var> ... ]</a></dt>
  <dd>
    <p>
    Het stuurcommando laat u toe om een bugrapport te dupliceren. Dit is nuttig
    in het geval één enkel bugrapport eigenlijk aangeeft dat zich verschillende
    bugs gemanifesteerd hebben. <q><var>Nieuwe ID's</var></q> zijn negatieve
    getallen, die door spaties gescheiden zijn en welke gebruikt kunnen worden
    in daaropvolgende stuurcommando's bij het verwijzen naar de pas
    gedupliceerde bugs. Voor elke nieuwe ID wordt een nieuw rapport gegenereerd.
    </p>

    <p>
    Gebruiksvoorbeeld:
    </p>

    <pre>
          clone 12345 -1 -2
          reassign -1 foo
          retitle -1 foo: foo is een ramp
          reassign -2 bar
          retitle -2 bar: bar is een ramp in combinatie met foo
          severity -2 wishlist
          clone 123456 -3
          reassign -3 foo
          retitle -3 foo: foo is een ramp
          merge -1 -3
    </pre>
  </dd>


  <dt><a name="merge"><code>merge</code> <var>bugnummer</var>
    <var>bugnummer</var> ...</a></dt>
  <dd>
    <p>
    Voegt twee of meer bugrapporten samen. Wanneer bugrapporten samengevoegd
    werden, heeft het sluiten, het als doorgestuurd markeren of een dergelijke
    markering ongedaan maken en het opnieuw toewijzen van een van deze bugs aan
    een nieuw pakket, een identiek effect op al de samengevoegde rapporten.
    </p>

    <p>
    Voordat bugs samengevoegd kunnen worden, moeten ze zich in exact dezelfde
    toestand bevinden: ofwel allemaal open of allemaal gesloten zijn, hetzelfde
    adres van de bovenstroomse auteur bevatten waarnaar de bug doorgestuurd
    werd, of allemaal niet als doorgestuurd gemarkeerd zijn, allemaal
    toegewezen zijn aan hetzelfde pakket of dezelfde pakketten (een exacte
    tekenreeksvergelijking wordt uitgevoerd op het pakket waaraan de bug werd
    toegewezen) en allemaal eenzelfde graad van ernstigheid hebben. Als ze zich
    bij aanvang niet in dezelfde staat bevinden, moet u <code>reassign</code>,
    <code>reopen</code> enzovoort gebruiken om ervoor te zorgen dat dit wel het
    geval is, voordat u <code>merge</code> gebruikt. Het is niet vereist dat
    titels overeenkomen en deze zullen niet beïnvloed worden door de
    samenvoeging. Het is evenmin vereist dat tags overeenkomen, deze zullen
    samengevoegd worden.
    </p>

    <p>
    Indien een van de bugs welke vermeld worden in een
    <code>merge</code>-commando reeds met een andere bug samengevoegd werd, dan
    worden alle rapporten die zijn samengevoegd met een van de vermelde
    bugs, allemaal samengevoegd. Samenvoegen is als gelijkheid: het is
    reflexief, transitief en symmetrisch.
    </p>

    <p>
    Het samenvoegen van rapporten doet een vermelding verschijnen in de
    logboeken van elk rapport. Op de WWW-pagina's houdt dit links naar de
    andere bugs in.
    </p>

    <p>
    Samengevoegde rapporten verlopen allemaal tegelijk, en enkel wanneer elk
    van de rapporten apart voldoet aan de criteria voor het verloop.
    </p>
  </dd>


  <dt><a name="forcemerge"><code>forcemerge</code> <var>bugnummer</var>
    <var>bugnummber</var> ...</a></dt>
  <dd>
    <p>
    Voegt dwingend twee of meer bugrapporten samen. De instellingen, welke in
    een gewone samenvoeging gelijk moeten zijn, worden ontleend aan de eerst
    vermelde bug en toegewezen aan de erna vermelde bugs. Om te vermijden dat
    door typefouten bugs per vergissing zouden samengevoegd worden, moeten de
    bugs zich in hetzelfde pakket bevinden. Bekijk bovenstaande tekst voor een
    beschrijving van wat samenvoegen betekent.
    </p>

    <p>
    Merk op dat dit het mogelijk maakt om bugs te sluiten door samenvoeging; u
    bent verantwoordelijk voor het informeren van indieners met een passend
    sluitingsbericht als u dit doet.
    </p>
  </dd>


  <dt><a name="unmerge"><code>unmerge</code> <var>bugnummer</var></a></dt>
  <dd>
    <p>
    Ontkoppelt een bugrapport van andere rapporten waarmee het mogelijk is
    samengevoegd. Indien het vermelde rapport samengevoegd is met verschillende
    andere, dan worden deze allemaal onderling samengevoegd gelaten. Enkel hun
    koppeling aan de expliciet vermelde bug wordt verwijderd.
    </p>

    <p>
    Indien verschillende bugrapporten samengevoegd zijn en u deze wenst op te
    splitsen in twee aparte groepen samengevoegde rapporten, dan moet u voor
    elk rapport afzonderlijk de samenvoeging in een van de nieuwe groepen
    opheffen en ze dan samenvoegen in de vereiste nieuwe groep.
    </p>

    <p>
    Met ieder <code>unmerge</code>-commando kunt u slechts voor één rapport de
    samenvoeging ongedaan maken; indien u meer dan één bug wilt ontkoppelen,
    moet u in uw bericht gewoon meerdere <code>unmerge</code>-commando's
    opnemen.
    </p>
  </dd>


  <dt><a name="tags"><!-- match tags too --></a><a name="tag"><code>tags</code> <var>bugnummer</var> [ <code>+</code> |
    <code>-</code> | <code>=</code> ] <var>tag</var> [ <var>tag</var>
    ... ] [ <code>+</code> | <code>-</code>
    | <code>=</code> <var>tag</var> ... ] ]</a></dt>
  <dd>
    <p>
    Stelt tags in voor het bugrapport #<var>bugnummer</var>. Er wordt geen
    kennisgeving gezonden aan de gebruiker die de bug rapporteerde. De actie
    instellen op <code>+</code> betekent dat elk erop volgende <var>tag</var>
    toegevoegd wordt, <code>-</code> betekent elk erop volgende <var>tag</var>
    verwijderen en <code>=</code> betekent de erop volgende tags instellen voor
    de opgegeven lijst. Tussenliggende <code>+</code>, <code>-</code> of
    <code>=</code> wijzigen de actie voor de erop volgende tags. De
    standaardactie is toevoegen.
    </p>

    <p>
    Gebruiksvoorbeeld:
    </p>

    <pre>
          \# hetzelfde als 'tags 123456 + patch'
          tags 123456 patch

          \# hetzelfde als 'tags 123456 + help security'
          tags 123456 help security

          \# de tags 'fixed' en 'pending' toevoegen
          tags 123456 + fixed pending

          \# de tag 'unreproducible' verwijderen
          tags 123456 - unreproducible

          \# de taglijst exact op 'moreinfo' en 'unreproducible' instellen
          tags 123456 = moreinfo unreproducible

	  \# de tag moreinfo verwijderen en een tag patch toevoegen
	  tags 123456 - moreinfo + patch
    </pre>

    <p>
    Momenteel kunnen de volgende tags gebruikt worden: <bts_tags>.
    </p>

    <p>
    Voor <a href="Developer#tags">hun betekenis</a> kunt u de
    algemene documentatie voor ontwikkelaars over het bugsysteem raadplegen.
    </p>
  </dd>


  <dt><a name="block"><code>block</code> <var>bugnummer</var> <code>by</code>
    <var>bug</var> ...</a></dt>
  <dd>
    Noteren dat de oplossing voor de eerste bug geblokkeerd wordt door de
    andere vermelde bugs.
  </dd>


  <dt><a name="unblock"><code>unblock</code> <var>bugnummer</var>
    <code>by</code> <var>bug</var> ...</a></dt>
  <dd>
    Noteren dat de oplossing voor de eerste bug niet langer geblokkeerd wordt
    door de andere vermelde bugs.
  </dd>


  <dt><a name="close"><code>close</code> <var>bugnummer</var> [
    <var>gerepareerde-versie</var> ] (verouderd)</a></dt>
  <dd>
    <p>
    Bugrapport #<var>bugnummer</var> sluiten.
    </p>

    <p>
    Er wordt een melding gestuurd naar de gebruiker die de bug heeft
    gerapporteerd, maar (in tegenstelling tot het zenden van een e-mail
    <var>bugnumber</var><code>-done@bugs.debian.org</code>) wordt de tekst van
    het e-mailbericht dat zorgde voor het sluiten van de bug,
    <strong>niet</strong> toegevoegd aan deze melding. De ontwikkelaar die een
    rapport sluit, moet er zelf voor zorgen dat de gebruiker die de bug
    rapporteerde, geïnformeerd wordt over de reden voor het sluiten ervan. Om
    die reden wordt het gebruik van dit commando als vervallen beschouwd.
    Raadpleeg de informatie voor ontwikkelaars over
    <a href="Developer#closing">hoe u een bug goed afsluit</a>.
    </p>

    <p>
    Indien u een <var>gerepareerde-versie</var> aanlevert, zal het
    bugvolgsysteem registreren dat de bug in die versie van het pakket
    gerepareerd werd.
    </p>
  </dd>


  <dt><a name="package"><code>package</code> [ <var>pakketnaam</var> ...
    ]</a></dt>
  <dd>
    <p>
    Beperkt de reikwijdte van de erop volgende commando's zodat deze enkel van
    toepassing zijn op de bugs die tegen de vermelde pakketten ingediend
    werden. U kunt één of meer pakketten vermelden. Indien u geen pakketten
    vermeldt, zullen de erop volgende commando's op alle bugs van toepassing
    zijn. U wordt aangemoedigd om dit te gebruiken als een veiligheidsfunctie
    voor het geval u per ongeluk de verkeerde bugnummers gebruikt.
    </p>

    <p>
    Gebruiksvoorbeeld:
    </p>

    <pre>
          package foo
          reassign 123456 bar 1.0-1

          package bar
          retitle 123456 bar: bar is een ramp
          severity 123456 normal

          package
          severity 234567 wishlist
    </pre>
  </dd>


  <dt><a name="owner"><code>owner</code> <var>bugnummer</var>
    <var>adres</var> | <code>!</code></a></dt>
  <dd>
    <p>
    Stelt in dat <var>adres</var> moet beschouwd worden als de <q>eigenaar</q>
    van #<var>bugnummer</var>. De eigenaar van een bug eist de
    verantwoordelijkheid op voor het repareren ervan. Dit is handig om werk te
    verdelen in gevallen waarin een pakket een team van beheerders heeft.
    </p>

    <p>
    Indien u zelf eigenaar wenst te worden van de bug, kunt u de snelnotatie
    <code>!</code> gebruiken of uw eigen e-mailadres opgeven.
    </p>
  </dd>


  <dt><a name="noowner"><code>noowner</code> <var>bugnummer</var></a></dt>
  <dd>
    Vergeet elk idee dat de bug een andere eigenaar heeft dan de gebruikelijke
    beheerder. Indien er geen eigenaar van de bug geregistreerd stond, doet dit
    niets.
  </dd>


  <dt><a name="archive"><code>archive</code> <var>bugnummer</var></a></dt>
  <dd>
    Archiveert een bug die op een bepaald moment in het verleden gearchiveerd
    was maar momenteel niet gearchiveerd is, als de bug voldoet aan de
    vereisten voor archivering, waarbij de tijd wordt genegeerd.
  </dd>


  <dt><a name="unarchive"><code>unarchive</code> <var>bugnummer</var></a></dt>
    <dd>
    Maakt de archivering van een bug, die voordien gearchiveerd was, ongedaan.
    Het ongedaan maken van een archivering zou over het algemeen gekoppeld
    moeten worden aan reopen en found/fixed naargelang het geval. Bugs waarvan
    de archivering ongedaan gemaakt werd, kunnen met het commando archive
    gearchiveerd worden, ervan uitgaande dat wordt voldaan aan de
    archiveringsvereisten welke niet gebaseerd zijn op de tijd. U zou het
    commando unarchive niet moeten gebruiken om triviale wijzigingen aan te
    brengen in gearchiveerde bugs, zoals het wijzigen van de indiener. Het
    primaire doel ervan is om het heropenen mogelijk te maken van bugs welke
    zonder tussenkomst van de BTS-beheerders gearchiveerd werden.
  </dd>


  <dt><a name="comment"><code>#</code>...</a></dt>
    <dd>
    Commentaar van één regel. Het <code>#</code>-teken moet aan het begin van
    de regel staan. De commentaartekst wordt toegevoegd aan de bevestiging die
    naar de verzender en de betrokken ontwikkelaars gezonden wordt, en dus kunt
    u dit gebruiken om de redenen voor de door u gebruikte commando's toe te
    lichten.
  </dd>


  <dt><a name="thanks"><code>quit</code></a></dt>
  <dt><code>stop</code></dt>
  <dt><code>thank</code></dt>
  <dt><code>thanks</code></dt>
  <dt><code>thankyou</code></dt>
  <dt><code>thank you</code></dt>
  <dt><code>--</code></dt>
  <!-- #366093, jou geef ik de schuld! -->
  <!-- <dt><code>kthxbye</code></dt> -->
  <!-- Zie... Ik heb het gedocumenteerd! -->
  <dd>
    In elk geval op een aparte regel, mogelijk gevolgd door witruimte, zegt
    dit de beheerserver om het verwerken van het bericht stop te zetten. De rest
    van het bericht kan verklaringen bevatten, handtekeningen of iets anders en
    niets daarvan zal door de beheerserver opgemerkt worden.
  </dd>
</dl>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
