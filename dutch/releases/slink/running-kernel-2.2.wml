#use wml::debian::template title="Errata: Linux 2.2.x gebruiken in slink"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<p>

Deze pagina documenteert bekende problemen met het gebruiken van de Linux 2.2.x
kernel in Debian 2.1 (slink). Er wordt verondersteld dat u een volledig
up-to-date slink-systeem gebruikt.

<p>

De release van slink is gecertificeerd en officieel getest voor gebruik met de
late 2.0.x Linux-kernels. Aangezien de bevriezing van Debian plaatsvond vóór de
release van Linux 2.2, en de wijziging van het hoofdversienummer van de kernel
complexe problemen kan veroorzaken, werd besloten om vast te houden aan de
beproefde 2.0-kernellijn.

<p>

De releases van Debian zijn echter niet noodzakelijk gebonden aan de versie van
de kernel. U kunt in Debian elke gewenste kernel gebruiken. We kunnen
eenvoudigweg echter geen garantie geven dat alles naar behoren zal
functioneren. Als u besluit over te stappen naar de Linux 2.2-serie en problemen
ondervindt met een pakket, hebt u misschien meer geluk wanneer u de versie van
dat pakket uit <a href="../potato/">potato</a> (Debian 2.2) gebruikt.

<p>

Hieronder staan veel links die verwijzen naar de potato-versie van pakketten.
Merk op dat als u deze pakketten op een stabiele machine installeert, u
mogelijk ook potato-bibliotheken of andere pakketvereisten zult moeten
installeren. Met name zult u waarschijnlijk uw libc6-pakket moeten upgraden. We
raden u aan om voor dit doel <code>apt-get</code> te gebruiken, dat bij een
correct gebruik alleen de benodigde pakketten ophaalt. Wees echter
gewaarschuwd: hoewel de meeste gebruikers geen problemen hebben met het gebruik
van een gemengd systeem van pakketten uit de stabiele en de bevroren
distributie, kunt u te maken krijgen met tijdelijke bugs in potato.

<p>

Het bronpakket <a href="https://packages.debian.org/kernel-source-2.2.1">
kernel-source-2.2.1</a> wordt in de distributie opgenomen om gebruikers te
ondersteunen die de 2.2.x versie van de Linux kernel willen gebruiken. Het
wordt echter aanbevolen om de standaardsites voor de kerneldistributie, zoals
<a href="https://www.kernel.org/">kernel.org</a>, te controleren op nieuwere
versies van de 2.2.x-broncodeboom en op extra errata. Er zijn bekende bugs in
2.2.1, en het is bekend dat die versie bij sommigen datacorruptie heeft
veroorzaakt. U zou de patches voor de nieuwere kernel uit de 2.2-serie moeten
halen en deze moeten toepassen op de broncodestructuur van de Linux-kernel.


<h2>Potentieel problematische pakketten</h2>

<p>

Houd er rekening mee dat deze lijst mogelijk onvolledig is. Dien een bug in
tegen www.debian.org als u andere problemen tegenkomt die niet in de lijst
staan. Controleer ook de buglogs voor het betreffende pakket; probeer er zeker
van te zijn dat het probleem werd geïntroduceerd in Linux 2.2.

<dl>
	<dt><a href="https://packages.debian.org/sysutils">sysutils</a>
	<dd>
<tt>procinfo</tt> kan niet uitgevoerd worden. De versie uit
<a href="https://www.debian.org/Packages/frozen/utils/sysutils.html">
potato</a> lost dit op.

	<dt><a href="https://packages.debian.org/netbase">netbase</a>
	<dd>
Het in Linux 2.2 aanwezige <tt>ipautofw</tt> moet vervangen worden door
<tt>ipmasqadm</tt>, en <tt>ipfwadm</tt> is vervangen door <tt>ipchains</tt>.
Het pakket
<a href="https://www.debian.org/Packages/frozen/base/netbase.html">
netbase</a> uit potato bevat een verpakkingsscript,
<tt>ipfwadm-wrapper</tt>, om de transitie te vergemakkelijken.
<p>
<tt>ifconfig</tt> zal geen aliasinterfaces tonen, en onder bepaalde
omstandigheden zal <tt>ipchains</tt> er stilzwijgend niet in slagen de
pakkettellers te wissen. Sommige routes die worden gebouwd door de init-scripts
van <tt>netbase</tt> zullen aanleiding geven tot ongevaarlijke
waarschuwingsberichten.
<p>
Al deze problemen werden opgelost in de versie van <a
href="https://www.debian.org/Packages/frozen/base/netbase.html">
potato</a>. Voor het geval u niet wenst op te waarderen naar potato, werden met
Debian 2.1 compatibele pakketten <a
href="https://www.debian.org/~rcw/2.2/netbase/">ter beschikking gesteld</a>.

	<dt><a href="https://packages.debian.org/pcmcia-source">pcmcia-source</a>
	<dd>
De versie van <tt>pcmcia-source</tt> in slink kan niet gecompileerd worden met
de 2.2 kernel. Opgelost in de versie van
<a href="https://www.debian.org/Packages/frozen/admin/pcmcia-source.html">
potato</a>.

	<dt><a href="https://packages.debian.org/dhcpcd">dhcpcd</a>
	<dd>
Raakt onklaar onder Linux 2.2; gebruik de versie uit <a
href="https://www.debian.org/Packages/frozen/net/dhcpcd.html">
potato</a>.

	<dt><a href="https://packages.debian.org/dhcp-client-beta">dhcp-client-beta</a>
	<dd>
Het <tt>/etc/dhclient-script</tt> werkt niet met 2.2. De versie uit
<a href="https://www.debian.org/Packages/frozen/net/dhcp-client.html">
potato</a> lost dit op; merk op dat het pakket hernoemd werd naar gewoonweg
<code>dhcp-client</code>.

	<dt><a href="https://packages.debian.org/wanpipe">wanpipe</a>
	<dd>
Versie 2.0.1, in slink, is incompatibel met de 2.2. kernels.
Versie 2.0.4 en hoger, die u kunt ophalen uit
<a href="https://www.debian.org/Packages/frozen/net/wanpipe.html">
potato</a>, werken voor de 2.2 kernels, maar niet met de 2.0
kernels (een kernel-patch voor 2.0 is echter wel opgenomen in de potato-versie).

	<dt><a href="https://packages.debian.org/netstd">netstd</a>
	<dd>
<tt>bootpc</tt> zal geen antwoord ontvangen tenzij de interface reeds
geconfigureerd werd. Dit werd opgelost in het nu aparte pakket <a
href="https://packages.debian.org/bootpc">bootpc</a> in potato.

	<dt><a href="https://packages.debian.org/lsof">lsof</a>
	<dd>
<tt>lsof</tt> moet opnieuw gecompileerd worden om te werken met linux 2.2.
Waardeer op naar het pakket <tt>lsof</tt> dat in potato te vinden is.

	<dt><a href="https://packages.debian.org/acct">acct</a>
	<dd>
De structuur voor het bijhouden van administratieve verrekeningen is gewijzigd
in de 2.2-kernel, dus als u <tt>acct</tt> en Linux 2.2 gebruikt, hebt u de
versie van het pakket uit potato nodig (deze is niet compatibel met de
2.0-serie van de kernel).

	<dt><a href="https://packages.debian.org/isdnutils">isdnutils</a>
	<dd>
<tt>isdnutils</tt> 3.0 en hoger in Debian zou zowel moeten werken met de
2.0-serie als met de 2.2-serie van de kernel. Dit is alleen het geval omdat de
pakketbeheerder van Debian speciale moeite heeft genomen om ervoor te zorgen
dat dit het geval is. Andere distributies hebben misschien niet zoveel geluk.

	<dt><a href="https://packages.debian.org/diald">diald</a>
	<dd>
Het pakket <tt>diald</tt> uit slink heeft problemen met het dynamisch creëren
van routes in Linux 2.2. Waardeer op naar de versie die in potato beschikbaar
is.

        <dt><a href="https://packages.debian.org/xosview">xosview</a>
	<dd>
<tt>xosview</tt> maakt een eindeloze lus met Linux 2.2.2 en hoger.
Waardeer op naar de versie die in potato beschikbaar is.



</dl>


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
