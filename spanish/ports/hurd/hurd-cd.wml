#use wml::debian::template title="Debian GNU/Hurd --- CD de Hurd" NOHEADER="yes"
#use wml::debian::translation-check translation="45b938d94cba8115131eaba8be06c7a978d40903"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"

<define-tag cdserie>L1</define-tag>
<define-tag cdbasetarball>gnu-2009-10-18.tar.gz</define-tag>
<define-tag cdbasename>debian-<cdserie>-hurd-i386</define-tag>

<h1>Debian GNU/Hurd</h1>

<p>Aunque mucha gente llama GNU/Hurd al sistema GNU, esto no es estrictamente cierto.
El núcleo es GNU Mach, no el Hurd. El Hurd consiste en una serie de servidores que
se ejecutan sobre el microkernel, GNU Mach. Tanto el Hurd como GNU Mach son
parte del proyecto GNU mientras que el núcleo Linux es un proyecto independiente.</p>

<p> El método más fácil (y probado) de probar Debian GNU/Hurd es usar una
máquina virtual a través de KVM. Algunas imágenes preinstaladas están disponibles en
<url "https://cdimage.debian.org/cdimage/ports/current-hurd-i386/README.txt">, pero también se puede
utilizar el instalador de Debian para instalar en KVM o una máquina nativa (pero el soporte para hardware
varía, por lo que es más recomendable intentarlo con KVM).
</p>

<h2>Usar el CD-ROM del instalador de Debian</h2>

<p>Se puede descargar una adaptación a hurd-i386 del instalador estándar de Debian
desde <url "https://cdimage.debian.org/cdimage/ports/current-hurd-i386/"> .
Asegúrese de leer el archivo README disponible junto con las imágenes ISO.
Funciona como la adaptación usual a Linux de instalador de Debian, es decir,
automáticamente, excepto unos pocos detalles:</p>

<ul>

<li>Asegúrese de activar el área de intercambio, de lo contrario Mach tendrá problemas en caso de que utilice
toda la memoria.</li>

<li>No monte una partición separada para <code>/usr</code>, de lo contrario el arranque
fallará.</li>

<li>
Lea <a href="hurd-install">las notas acerca de la instalación manual</a> 
que documentan algunos de los pasos finales de la configuración.
</li>

</ul>

<p>Se pueden encontrar instrucciones para grabar CD de las imágenes en 
las <a href="$(HOME)/CD/faq/">Preguntas frecuentes sobre CD de Debian CD</a>.</p>

<h2>Instantáneas más recientes</h2>

<p>En <url "https://cdimage.debian.org/cdimage/ports/latest/hurd-i386/"> hay disponibles algunas instantáneas más recientes.</p>

<p>En <url "https://people.debian.org/~sthibault/hurd-i386/installer/cdimage/"> hay instantáneas diarias (¡sin probar!).</p>

<h2>Crear un disco de arranque GRUB</h2>

<p>
Si está instalando Hurd como único sistema, puede dejar que el instalador instale GRUB por sí solo.
Pero si está instalando Hurd junto con un sistema existente,
muy probablemente desee  elegir entre los dos. Si su sistema existente
es Linux, probablemente pueda simplemente ejecutar update-grub y eso detectara su nuevo sistema Hurd instalado.
En otro caso, si no consigue arrancar con Hurd de ese modo, puede usar un disco de arranque  GRUB.</p>

<p>
Instale el paquete grub-disk o grub-rescue-pc, contienen una imagen de disquete de GRUB.
Puede usar «dd» si está trabajando en GNU/Linux o «rawrite» en caso que este trabajando en Windows.
</p>

<p>
Asegúrese de entender los métodos que usan Linux, GRUB y Hurd
para nombrar de dispositivos y particiones. Utilizará los tres
y la relación entre ellos puede ser confusa.
</p>

<p>Hurd usa nombres de particiones diferentes a los de Linux, por eso debe tener cuidado.
Los Discos Duros IDE son numerados en el orden, empezando desde hd0 para el primario maestro
y si es esclavo hd1, seguido por el secundario maestro hd2 y su esclavo hd3.
Los dispositivos SCSI también se enumeran en orden absoluto.
Siempre serán sd0, sd1 y así sucesivamente independientemente si las dos unidades son 
SCSI id 4 y 5 o como sea. La experiencia ha  demostrado que las unidades de CD-ROM
pueden ser mas complicadas. Más acerca de esto más adelante.</p>

<p>Las particiones al estilo Linux siempre se llaman sn cuando usa Hurd,
donde n es el número de partición, así la primera partición del primer disco IDE 
será hd0s1, la tercera partición en el segundo disco SCSI será sd1s3 y así sucesivamente.</p>

<p> GRUB1 tiene otro sistema de nombres de particiones. Llama particiones (hdN, n),
pero esta vez el número de disco y el número de partición comienzan ambos desde cero, y
los discos van en orden, todos los discos IDE primero, y los SCSI después. Esta
vez, la primera partición en la primera unidad IDE será (hd0,0). GRUB2 hace
lo mismo, pero el número de partición comienza por uno, por lo que en ese caso será
(hd0,1). Para causar realmente una confusión, (hd1,2) podría referirse a la primera 
unidad SCSI si sólo tiene una unidad IDE, o podría referirse a la segunda unidad IDE.
Por lo tanto, es importante que haya averiguado los distintos nombres de sus particiones
antes de empezar.</p>

<p>Disfrute de Hurd.</p>
