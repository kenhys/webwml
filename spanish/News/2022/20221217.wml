#use wml::debian::translation-check translation="f878955e279294523a604a4e9bbd3d93dd0f3cc7"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.6</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la sexta actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction awstats "Corrige problema de ejecución de scripts entre sitios («cross site scripting») [CVE-2022-46391]">
<correction base-files "Actualiza /etc/debian_version para la versión 11.6">
<correction binfmt-support "Ejecuta binfmt-support.service después de systemd-binfmt.service">
<correction clickhouse "Corrige problemas de lectura fuera de límites [CVE-2021-42387 CVE-2021-42388] y problemas de desbordamiento de memoria [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "Extensión («plugin») CRI: corrige fuga de memoria en goroutine durante Exec [CVE-2022-23471]">
<correction core-async-clojure "Corrige fallos de compilación en el juego de pruebas">
<correction dcfldd "Corrige la salida SHA1 en arquitecturas big-endian">
<correction debian-installer "Recompilado contra proposed-updates; incrementa la ABI del núcleo Linux a la 5.10.0-20">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debmirror "Añade non-free-firmware («firmware no libre») a la lista de secciones por omisión">
<correction distro-info-data "Añade Ubuntu 23.04, Lunar Lobster; actualiza fechas de fin de Debian ELTS; corrige la fecha de publicación de Debian 8 (jessie)">
<correction dojo "Corrige problema de contaminación de prototipo [CVE-2021-23450]">
<correction dovecot-fts-xapian "Genera dependencia con la versión de la ABI de dovecot en uso en el momento de compilar">
<correction efitools "Corrige fallo intermitente de compilación debido a dependencia incorrecta en el makefile">
<correction evolution "Mueve las libretas de direcciones de Google Contacts a CalDAV porque la API de Google Contacts ha sido desactivada">
<correction evolution-data-server "Mueve las libretas de direcciones de Google Contacts a CalDAV porque la API de Google Contacts ha sido desactivada; corrige compatibilidad con los cambios en Gmail OAuth">
<correction evolution-ews "Corrige la recuperación de certificados de usuario pertenecientes a contactos">
<correction g810-led "Controla acceso a dispositivos con uaccess en lugar de darles permisos universales de escritura [CVE-2022-46338]">
<correction glibc "Corrige regresión en wmemchr y en wcslen en las CPU con AVX2 pero sin BMI2 (p. ej. Intel Haswell)">
<correction golang-github-go-chef-chef "Corrige fallo intermitente en las pruebas">
<correction grub-efi-amd64-signed "No aplica dh_strip a los binarios de Xen, haciendo que vuelvan a funcionar; incluye tipos de letra en la compilación de memdisk para imágenes EFI; corrige fallo en el código para volcados de memoria de forma que se gestionen mejor los errores; incrementa el nivel de Debian SBAT a 4">
<correction grub-efi-arm64-signed "No aplica dh_strip a los binarios de Xen, haciendo que vuelvan a funcionar; incluye tipos de letra en la compilación de memdisk para imágenes EFI; corrige fallo en el código para volcados de memoria de forma que se gestionen mejor los errores; incrementa el nivel de Debian SBAT a 4">
<correction grub-efi-ia32-signed "No aplica dh_strip a los binarios de Xen, haciendo que vuelvan a funcionar; incluye tipos de letra en la compilación de memdisk para imágenes EFI; corrige fallo en el código para volcados de memoria de forma que se gestionen mejor los errores; incrementa el nivel de Debian SBAT a 4">
<correction grub2 "No aplica dh_strip a los binarios de Xen, haciendo que vuelvan a funcionar; incluye tipos de letra en la compilación de memdisk para imágenes EFI; corrige fallo en el código para volcados de memoria de forma que se gestionen mejor los errores; incrementa el nivel de Debian SBAT a 4">
<correction hydrapaper "Añade dependencia con python3-pil, que faltaba">
<correction isoquery "Corrige fallo en una prueba provocado por una modificación de la traducción al francés en el paquete iso-codes">
<correction jtreg6 "Nuevo paquete, necesario para compilar versiones más recientes de openjdk-11">
<correction lemonldap-ng "Mejora la propagación de la destrucción de sesiones [CVE-2022-37186]">
<correction leptonlib "Corrige división por cero [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Corrige problema de redirección abierta [CVE-2021-3639]">
<correction libbluray "Corrige soporte de BD-J con actualizaciones recientes de Oracle Java">
<correction libconfuse "Corrige una lectura de memoria dinámica («heap») fuera de límites en cfg_tilde_expand [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libtasn1-6 "Corrige problema de lectura fuera de límites [CVE-2021-46848]">
<correction libvncserver "Corrige fuga de memoria [CVE-2020-29260]; soporta tamaños de pantalla mayores">
<correction linux "Nueva versión «estable» del proyecto original; incrementa la ABI a la 20; [rt] actualiza a la 5.10.158-rt77">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 20; [rt] actualiza a la 5.10.158-rt77">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 20; [rt] actualiza a la 5.10.158-rt77">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; incrementa la ABI a la 20; [rt] actualiza a la 5.10.158-rt77">
<correction mariadb-10.5 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "Descarta la cabecera X-Client-IP cuando no es una cabecera confiable [CVE-2022-2255]">
<correction mplayer "Corrige varios problemas de seguridad [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "Corrige caída de gpgme al listar las claves de un bloque de claves públicas y corrige el listado de bloques de claves públicas con versiones antiguas de gpgme">
<correction nano "Corrige caídas y un problema de potencial pérdida de datos">
<correction nftables "Corrige error por uno («off-by-one») / «doble liberación»">
<correction node-hawk "Analiza sintácticamente las URL utilizando stdlib [CVE-2022-29167]">
<correction node-loader-utils "Corrige problema de contaminación de prototipo [CVE-2022-37599 CVE-2022-37601] y problema de denegación de servicio relacionado con expresiones regulares [CVE-2022-37603]">
<correction node-minimatch "Mejora la protección frente a denegación de servicio relacionada con expresiones regulares [CVE-2022-3517]; corrige regresión en parche para CVE-2022-3517">
<correction node-qs "Corrige problema de contaminación de prototipo [CVE-2022-24999]">
<correction node-xmldom "Corrige problema de contaminación de prototipo [CVE-2022-37616]; evita inserción de nodos mal construidos [CVE-2022-39353]">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Añade al paquete omnievents-doc dependencia con libjs-jquery, que faltaba">
<correction onionshare "Corrige problema de denegación de servicio [CVE-2022-21689] y problema de inyección de HTML [CVE-2022-21690]">
<correction openvpn-auth-radius "Soporta la directiva verify-client-cert">
<correction postfix "Nueva versión «estable» del proyecto original">
<correction postgresql-13 "Nueva versión «estable» del proyecto original">
<correction powerline-gitstatus "Corrige inyección de órdenes mediante configuración maliciosa de repositorio [CVE-2022-42906]">
<correction pysubnettree "Corrige compilación del módulo">
<correction speech-dispatcher "Reduce el tamaño de las zonas de memoria de espeak para evitar artefactos en la síntesis">
<correction spf-engine "Corrige fallo de arranque de pyspf-milter debido a una sentencia import inválida">
<correction tinyexr "Corrige problemas de desbordamiento de memoria dinámica («heap») [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Corrige bucle infinito [CVE-2021-42260]">
<correction tzdata "Actualiza datos para Fiyi, México y Palestina; actualiza lista de segundos intercalares">
<correction virglrenderer "Corrige problema de escritura fuera de límites [CVE-2022-0135]">
<correction x2gothinclient "Hace que el paquete x2gothinclient-minidesktop proporcione el paquete virtual lightdm-greeter">
<correction xfig "Corrige problema de desbordamiento de memoria [CVE-2021-40241]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>



<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>

