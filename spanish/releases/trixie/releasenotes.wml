#use wml::debian::template title="Debian 13 -- Notas de publicación" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="bd44c0367d403f4c6efaa83c6373a24a7991a1bb"

<if-stable-release release="buster">
<p>Todavía no hay notas de publicación para Debian 13, nombre en clave trixie.</p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>Esta es una <strong>versión de trabajo</strong> de las notas de publicación
para Debian 13, nombre en clave trixie, que aún no se ha publicado. La
información que se presenta aquí podría estar desactualizada y ser inexacta y,
seguramente, incompleta.</p>
</if-stable-release>

<p>Para conocer las novedades de Debian 13 consulte las notas de publicación
para su arquitectura:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Notas de publicación'); :>
</ul>

<p>Las notas de publicación también contienen instrucciones para los usuarios que van a actualizar el sistema
desde versiones anteriores.</p>

<p>Si ha configurado adecuadamente las opciones de localización de su
navegador, podrá usar el enlace anterior para acceder automáticamente a la versión
HTML correcta &mdash; consulte la información sobre <a href="$(HOME)/intro/cn">negociación de contenido</a>.
Si no es así, elija en la tabla siguiente la arquitectura, formato e
idioma concretos que desee.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitectura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
