#use wml::debian::translation-check translation="d3f29d8015c29a9da9f70c50fcc3cb13a49a95c7"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron múltiples vulnerabilidades en cURL, una biblioteca para
transferencia de URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5436">CVE-2019-5436</a>

    <p>Se descubrió un desbordamiento de memoria dinámica («heap») en el código de recepción de TFTP
    que podría permitir denegación de servicio o ejecución de código arbitrario. Esto solo afecta
    a la distribución «antigua estable» (stretch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5481">CVE-2019-5481</a>

    <p>Thomas Vegas descubrió una «doble liberación» en el código de FTP-KRB desencadenada
    por el envío de un bloque de datos muy grande por parte de un servidor malicioso.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

    <p>Thomas Vegas descubrió un desbordamiento de memoria dinámica («heap») que podría
    desencadenarse cuando se utiliza un tamaño de bloque de TFTP pequeño y distinto del tamaño de bloque por omisión.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 7.52.1-5+deb9u10.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 7.64.0-4+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de curl.</p>

<p>Para información detallada sobre el estado de seguridad de curl, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4633.data"
